#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   make_efficiency_plots.py
# @author Clara Remon Alepuz (cremonal@cern.ch)
# @based  on Albert Puig code
# @date   28.03.2020
# =============================================================================
"""Make efficiency plots from calibration samples."""
from itertools import tee, izip, cycle
import os
import sys

import ROOT
import array

from IsNotXCalib.utils import pairwise, get_colors, get_weighted_entries, efficiency
from IsNotXCalib.binning import load_binning_from_txt
from IsNotXCalib.defaults import apply_defaults, add_file_info
from IsNotXCalib.calibration.build_efficiency_tables import get_efficiency_table


def make_graph(binning, x_axis, file_name, legend_header=None, **points):
    """Make the efficiency graph.

    :param binning: Binning in pt.
    :type binning: list
    :param points: Dictionary of data sets
    :type points: dict

    :returns: ROOT.TGraph

    :raises: ValueError: In case of mismatches in lengths of input lists.

    """
    colors = cycle([ROOT.kBlue, ROOT.kTeal,
                  ROOT.kMagenta,
                  ROOT.kBlack,
                  ROOT.kRed,
                  ROOT.kGray,
                  ROOT.kMagenta,
                  ROOT.kCyan,
                  ROOT.kOrange,
                  ROOT.kSpring,
                  ROOT.kYellow,
                  ROOT.kTeal,
                  ROOT.kViolet,
                  ROOT.kAzure,
                  ROOT.kPink])

    n_points = len(binning) - 1
    # Binning
    x_binning = [(up+low)/2.0 for low, up in pairwise(binning)]
    x_err_binning = [(up-low)/2.0 for low, up in pairwise(binning)]
    # Let's build the graph
    graph = ROOT.TMultiGraph()
    # Prepare legend
    legend_height = len(points) * 0.06
    #print 'points values', points.values()
    avg_y_low = sum([point_set[0][0] for point_set in points.values()])/len(points)
    if avg_y_low > 0.5:
        legend_y_start = 0.15
    else:
        legend_y_start = 0.89 - legend_height
    legend = ROOT.TLegend(0.15, legend_y_start, 0.5, legend_y_start + legend_height, "", "brNDC")
    legend.SetFillColor(0)
    if legend_header:
        legend.SetHeader(legend_header)
    # Fill
    graphs = []
    for name in sorted(points.keys()):
        points_data = points[name]
        print points_data
        print n_points
        if n_points != len(points_data):
            raise ValueError("Mismatch in input size!")
        y_data = [point[0] for point in points_data]
        y_err_data = [point[1] for point in points_data]
        graph_data = ROOT.TGraphErrors(n_points)
        graph_data.SetMarkerStyle(21)
        graph_data.SetMarkerSize(0.7)
        color = next(colors)
        graph_data.SetLineColor(color)
        graph_data.SetMarkerColor(color)
        # Fill
        for i in range(n_points):
            graph_data.SetPoint(i, x_binning[i], y_data[i])
            graph_data.SetPointError(i, x_err_binning[i], y_err_data[i])
        graphs.append(graph_data)
        graph.Add(graph_data)
        legend.AddEntry(graph_data, name, 'lp')
    # And put in TMultiGraph
    canvas = ROOT.TCanvas()
    graph.Draw("AP")
    graph.GetXaxis().SetTitle(x_axis)
    graph.GetYaxis().SetTitle("Efficiency")
    if len(points) > 1:
        legend.Draw()
    graph.GetYaxis().SetRangeUser(0.0, 1.05)
    canvas.Update()
    print "Saving efficiency plot in %s .eps" % file_name
    print "Saving efficiency plot in %s .root" % file_name

    canvas.SaveAs(file_name+".eps")
    canvas.SaveAs(file_name+".root")



def plot_binvar(tree, cuts, bins_1, bins_2, bin_var, args):
    # Build efficiencies
    binvardep = args.plot_type
    if(binvardep == 'PT'):
        bin_var2plot = args.pt_var
    elif(binvardep == 'ETA'):
        bin_var2plot = args.eta_var
    elif(binvardep == 'NSPDHits'):
        bin_var2plot = args.nspdhits_var
    elif(binvardep == 'NPVs'):
        bin_var2plot = args.npvs_var
    elif(binvardep == 'NTracks'):
        bin_var2plot = args.ntracks_var
    bins_1 = [0.0, 100000.0]
    nobins, bins_2 = load_binning_from_txt(bin_file, ('---', binvardep))
    data = {}
    for cut in cuts:
        if(args.IsNotX == "IsNotH"):
            table = get_efficiency_table(ref_tree, args.stripping_version, args.isphoton_cut, cut, args.isnote_cut, bins_1, bins_2,
                                         isphoton_var=args.isphoton_var,
                                         isnoth_var=args.isnoth_var,
                                         isnote_var=args.isnote_var,
                                         main_var=args.IsNotX,
                                         weight_var=args.weight_var,
                                         pt_var=bin_var[0],
                                         eta_var=bin_var2plot)
            data['IsNotH > %s' % cut] = [val for _, val in table.get_points()]
        else:
            table = get_efficiency_table(ref_tree, args.stripping_version, args.isphoton_cut, args.isnoth_cut, cut, bins_1, bins_2,
                                         isphoton_var=args.isphoton_var,
                                         isnoth_var=args.isnoth_var,
                                         isnote_var=args.isnote_var,
                                         main_var=args.IsNotX,
                                         weight_var=args.weight_var,
                                         pt_var=bin_var[0],
                                         eta_var=bin_var2plot)
            data['IsNotE > %s' % cut] = [val for _, val in table.get_points()]
    # Make graph
    file_name = '%s_efficiency_vs_%s' % (args.IsNotX, binvardep)
    print "bins pt", bin_var[0]
    print "bins eta", bin_var[1]
    make_graph(bins_2, binvardep , file_name, args.calib_sample, **data)


def plot_2D(tree,  bins_pt, bins_eta, bin_var, args):
    ROOT.gStyle.SetOptStat('')
    isphoton_var=args.isphoton_var
    isnoth_var=args.isnoth_var
    isnote_var=args.isnote_var
    main_var=args.IsNotX
    pt_var = bin_var[0]
    eta_var = bin_var[1]
    isphoton_var = args.isphoton_var
    weight_var = args.weight_var

    # Base cut
    if(main_var=="IsNotE"):

        if args.stripping_version == 'S20':
            base_cut = ROOT.TCut('({0} != 0.5) && ({0} > {1}) && ({2} > {3}) '.format(isnoth_var, args.isnoth_cut, isphoton_var, args.isphoton_cut))
        else:
            base_cut = ROOT.TCut('({0} > {1}) && ({2} > {3})'.format(isphoton_var, args.isphoton_cut, isnoth_var, args.isnoth_cut))
    else:

        base_cut = ROOT.TCut('({0} > {1}) && ({2} > {3}) '.format(isnote_var, args.isnote_cut, isphoton_var, args.isphoton_cut))

    efficiency = {}
    for lower_pt, upper_pt in pairwise(bins_pt):
        for lower_eta, upper_eta in pairwise(bins_eta):
            cut = ROOT.TCut(base_cut)
            cut += ROOT.TCut('({0} >= {1} && {0} < {2} && {3} >= {4} && {3} < {5})'.format(pt_var, lower_pt, upper_pt, eta_var, lower_eta, upper_eta))
            efficiency_denominator = get_weighted_entries(tree, cut.GetTitle(), weight_var)
            cut_isphoton = ROOT.TCut(cut)
            if(main_var=="IsNotE"):
                cut_isphoton += ROOT.TCut('(%s > %s)' % (isnote_var, args.isnote_cut))
            else:
                if args.stripping_version == 'S20':
                    cut_isphoton += ROOT.TCut('({0} != 0.5) && (%s > %s)' % (isnoth_var, args.isnoth_cut))
                else:
                    cut_isphoton += ROOT.TCut('(%s > %s)' % (isnoth_var, args.isnoth_cut))
            efficiency_numerator = get_weighted_entries(tree, cut_isphoton.GetTitle(), weight_var)
            if float(efficiency_denominator) != 0.0:
                efficiency[str(lower_pt)+'_'+str(lower_eta)] = float(efficiency_numerator)/float(efficiency_denominator)
            else:
                efficiency[str(lower_pt)+'_'+str(lower_eta)] = 0.0
    # Fill histogram
    array_pt = array.array('f', bins_pt)
    array_eta = array.array('f', bins_eta)
    histo = ROOT.TH2F(" ", " ", len(bins_pt)-1, array_pt, len(bins_eta)-1, array_eta)
    histoxx = histo.GetXaxis()
    histoyy = histo.GetYaxis()
    nxx = histoxx.GetNbins()
    nyy = histoyy.GetNbins()
    for binY in range(nyy):
        for binX in range(nxx):
            histo.Fill(bins_pt[binX]+1, bins_eta[binY]+0.01, efficiency[str(bins_pt[binX])+'_'+str(bins_eta[binY])])
    histo.GetXaxis().SetTitle('Transverse momentum (MeV)')
    histo.GetYaxis().SetTitle('Pseudorapidity')
    canvas = ROOT.TCanvas()
    canvas.cd()
    histo.Draw('colz')
    histo.Draw('text same')
    plotname = 'efficiency_%s_%s_' % (bin_var[0], bin_var[1])
    canvas.SaveAs(plotname+'.eps')
    canvas.SaveAs(plotname+'.root')


def plot_isNotX(tree, args):
    # Build efficiencies
    data = {'points': []}
    isphoton_cuts = [i/20.0 for i in range(22)]
    n_entries = get_weighted_entries(tree, '1', args.weight_var)
    if(args.IsNotX=="IsNotE"):

        if args.stripping_version == 'S20':
            base_cut = ROOT.TCut('({0} != 0.5) && ({0} > {1}) && ({2} > {3}) '.format(args.isnoth_var, args.isnoth_cut, args.isphoton_var, args.isphoton_cut))
        else:
            base_cut = ROOT.TCut('({0} > {1}) && ({2} > {3})'.format(args.isphoton_var, args.isphoton_cut, args.isnoth_var, args.isnoth_cut))
    else:

        base_cut = ROOT.TCut('({0} > {1}) && ({2} > {3}) '.format(args.isnote_var, args.isnote_cut, args.isphoton_var, args.isphoton_cut))

    for cut_low, cut_high in pairwise(isphoton_cuts):
        cut = ROOT.TCut(base_cut)
        main_cut = (cut_low + cut_high)/2.0
        cut_isphoton = ROOT.TCut(cut)
        if(args.IsNotX=="IsNotE"):
            cut_isphoton += ROOT.TCut('(%s > %s)' % (args.isnote_var, main_cut))
        else:
            if args.stripping_version == 'S20':
                cut_isphoton += ROOT.TCut('({0} != 0.5) && (%s > %s)' % (args.isnoth_var, main_cut))
            else:
                cut_isphoton += ROOT.TCut('(%s > %s)' % (args.isnoth_var, main_cut))

        n_entries = get_weighted_entries(tree, cut.GetTitle(), args.weight_var)
        n_entries_isphoton = get_weighted_entries(tree, cut_isphoton.GetTitle(), args.weight_var)
        data['points'].append(efficiency(n_entries_isphoton, n_entries))

    # Make graph
    file_name = 'efficiency_vs_%s_' % args.IsNotX
    make_graph(isphoton_cuts, str(args.IsNotX), file_name, args.calib_sample, **data)


if __name__ == '__main__':
    import argparse
    # Define parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--binfile', default='', type=str,
                        help='Location of the binning file')
    parser.add_argument('-e', '--energy_range', type=str, default = 'range5', dest='energy_range',
                        help='Energy range', choices = ['range1', 'range2', 'range3', 'range4', 'range5', 'user', 'range7', 'range8_ptmin', 'range8_ptmax','range9'])
    parser.add_argument('--stripping', type=str, default='S21', dest='stripping_version',
                        help='Stripping version of the calibration samples', choices = ['S20', 'S21', 'TURCAL', 'S24', 'S28', 'S29', 'S34'])

    parser.add_argument('--plot_type', type=str, choices=['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks', '2D','isnotx'],
                        help='Type of plot to make')

    parser.add_argument('--var_eff', type=str, default='IsNotH', dest='IsNotX',
                                                                                                help='Variable to compute the efficiencies', choices = ['IsNotH', 'IsNotE'])
    parser.add_argument('--isnothcut', type=float, default = 0.0, dest='isnoth_cut',
                                                    help='Cuts to apply to the IsNotH variable')
    parser.add_argument('--isnotecut', type=float, default = 0.0, dest='isnote_cut',
                                                    help='Cuts to apply to the IsNotE variable')
    parser.add_argument('--isphotoncut', type=float, default=0.0, dest='isphoton_cut',
                        help='Cut to apply to the isPhoton variable')

    parser.add_argument('-c', '--calibsample', type=str,  dest='calib_sample',
                        help='Calibration sample')

    parser.add_argument('--binvar_1', type=str, default='PT', dest='bin1',
    help='Binning variable 1', choices = ['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks'])
    parser.add_argument('--binvar_2', type=str, default='ETA', dest='bin2',
    help='Binning variable 2', choices = ['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks'])

    # Parse
    args       = parser.parse_args()
    args.usemc = False
    erange     = args.energy_range
    args       = apply_defaults(add_file_info(args, erange), erange, 'calibrate')
    file_name  = args.input_file_name
    tree_name  = args.input_tree_name
    bin_file   = args.binfile
    print "binfile" , bin_file
    isnotx     = args.IsNotX
    binvars    = [args.bin1,args.bin2]
    bin_var = ['', '']
    for i in [0,1]:
        if(binvars[i] == 'PT'):
            bin_var[i] = args.pt_var
        elif(binvars[i] == 'ETA'):
            bin_var[i] = args.eta_var
        elif(binvars[i] == 'NSPDHits'):
            bin_var[i] = args.nspdhits_var
        elif(binvars[i] == 'NPVs'):
            bin_var[i] = args.npvs_var
        elif(binvars[i] == 'NTracks'):
            bin_var[i] = args.ntracks_var

    # Open and check reference file
    if not os.path.exists(file_name):
        print "Cannot find the file with the reference tree -> %s" % file_name
        sys.exit(1)
    ref_file = ROOT.TFile(file_name, 'r')
    ref_tree = ref_file.Get(tree_name)
    if not ref_tree:
        print "The reference file does not contain the specified tree -> %s" % tree_name
        sys.exit(1)
    if not os.path.exists(file_name):
        print "Cannot find the file with the reference tree -> %s" % file_name
        sys.exit(1)
    # Load binning
    try:
        bins_pt, bins_eta = load_binning_from_txt(bin_file, binvars)
    except OSError:
        print "Cannot load binning scheme from %s" % bin_file
        sys.exit(1)
    # Choose appropriate function depending on plot type

    if(isnotx=="IsNotH"):
        cuts = [0.5, 0.6, 0.7, 0.8, 0.9]
    if(isnotx=="IsNotE"):
        cuts = [0.2, 0.25, 0.3, 0.35, 0.4]


    if args.plot_type == 'isnotx':
        plot_isNotX(ref_tree, args)
    elif args.plot_type == '2D':
        plot_2D(ref_tree,  bins_pt, bins_eta, bin_var, args)
    else:
        plot_binvar(ref_tree, cuts, bins_pt, bins_eta, bin_var, args)


# EOF
