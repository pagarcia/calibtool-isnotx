#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   build_efficiency_tables.py
# @author Clara Remon Alepuz (cremonal@cern.ch)
# @based  on Albert Puig code
# @date   28.03.2020
# =============================================================================
"""Build efficiency tables (for MC and data) for the pi0/gamma separation tool."""

import os
import sys

import ROOT
from IsNotXCalib.binning import load_binning_from_txt
from IsNotXCalib.defaults import apply_defaults, add_file_info
from IsNotXCalib.utils import pairwise, get_weighted_entries, efficiency
from IsNotXCalib.calibration.calibration_table import CalibrationTable


def get_efficiency_table(tree, str_v, isphoton_cut, isnoth_cut, isnote_cut, bins_pt, bins_eta, **var_defs):
    """Get efficiencies as a function of the particle pt.

    :param tree: Reference tree.
    :type tree: ROOT.TTree
    :param str_v: stripping version used.
    :type str_v: string
    :param isphoton_cut: Cut in gamma/pi0 separation to apply.
    :type isphoton_cut: float
    :param isnoth_cut: Cut in IsNotH separation to apply.
    :type isnoth_cut: float
    :param isnote_cut: Cut in IsNotE separation to apply.
    :type isnote_cut: float
    :param binning: Pt binning to use.
    :type binning: list
    :param var_defs: Names of variables in the input tree.
    :type var_defs: dict


    """
    # Base cut
    if(var_defs['main_var']=="IsNotE"):

        if str_v == 'S20':
            base_cut = ROOT.TCut('({0} != 0.5) && ({0} > {1}) && ({2} > {3}) '.format(var_defs['isnoth_var'], isnoth_cut, var_defs['isphoton_var'], isphoton_cut))
        else:
            base_cut = ROOT.TCut('({0} > {1}) && ({2} > {3})'.format(var_defs['isphoton_var'], isphoton_cut, var_defs['isnoth_var'], isnoth_cut))
    else:

        base_cut = ROOT.TCut('({0} > {1}) && ({2} > {3}) '.format(var_defs['isnote_var'], isnote_cut, var_defs['isphoton_var'], isphoton_cut))


    # Loop
    output = {}
    for lower_pt, upper_pt in pairwise(bins_pt):
        for lower_eta, upper_eta in pairwise(bins_eta):
            cut = ROOT.TCut(base_cut)

            cut += ROOT.TCut('{0} >= {1} && {0} < {2} && {3} >= {4} && {3} < {5}'.format(var_defs['pt_var'], lower_pt, upper_pt, var_defs['eta_var'], lower_eta, upper_eta))



            cut_isphoton = ROOT.TCut(cut)
            if(var_defs['main_var']=="IsNotE"):
                cut_isphoton += ROOT.TCut('(%s > %s)' % (var_defs['isnote_var'], isnote_cut))
            else:
                if str_v == 'S20':
                    cut_isphoton += ROOT.TCut('({0} != 0.5) && (%s > %s)' % (var_defs['isnoth_var'], isnoth_cut))
                else:
                    cut_isphoton += ROOT.TCut('(%s > %s)' % (var_defs['isnoth_var'], isnoth_cut))

            #tree.Print()
            n_total = get_weighted_entries(tree, "gamma_PT>0", var_defs['weight_var'])
            n_entries = get_weighted_entries(tree, cut.GetTitle(), var_defs['weight_var'])
            n_entries_isphoton = get_weighted_entries(tree, cut_isphoton.GetTitle(), var_defs['weight_var'])
            output[(lower_pt, upper_pt, lower_eta, upper_eta)] = efficiency(n_entries_isphoton, n_entries)
    return CalibrationTable(output)


if __name__ == '__main__':
    import argparse
    # Define parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--binfile', default='', type=str,
                        help='Location of the binning file')
    parser.add_argument('--isphotoncut', type=float, default=0.0, dest='isphoton_cut',
                        help='Cut to apply to the isPhoton variable')
    parser.add_argument('--usemc', action='store_true', default=False,
                        help='Use the MC calibration samples')
    parser.add_argument('-o', '--output', action='append', choices=['text', 'cpp', 'python'],
                        help='Output formats of the calibration table')
    parser.add_argument('-e', '--energy_range', type=str, default = 'range5', dest='energy_range',
                        help='Energy range', choices = ['range1', 'range2', 'range3', 'range4', 'range5', 'user', 'range6', 'range7', 'range8_ptmin', 'range8_ptmax','range9'])
    parser.add_argument('--stripping', type=str, default='S21', dest='stripping_version',
                        help='Stripping version of the calibration samples', choices = ['S20', 'S21', 'TURCAL', 'S24', 'S28', 'S29', 'S34'])

    parser.add_argument('--var_eff', type=str, default='IsNotH', dest='IsNotX',
                                                                        help='Variable to compute the efficiencies', choices = ['IsNotH', 'IsNotE'])
    parser.add_argument('--isnothcut', type=float, default = 0.0, dest='isnoth_cut',
                            help='Cuts to apply to the IsNotH variable')
    parser.add_argument('--isnotecut', type=float, default = 0.0, dest='isnote_cut',
                            help='Cuts to apply to the IsNotE variable')

    parser.add_argument('--binvar_1', type=str, default='PT', dest='bin1',
        help='Binning variable 1', choices = ['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks'])
    parser.add_argument('--binvar_2', type=str, default='ETA', dest='bin2',
        help='Binning variable 2', choices = ['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks'])

    # Parse
    args        = parser.parse_args()
    erange    = args.energy_range
    args        = apply_defaults(add_file_info(args, erange), erange, 'calibrate')
    file_name   = args.input_file_name
    tree_name   = args.input_tree_name
    bin_file    = args.binfile
    binvars    = [args.bin1,args.bin2]
    bin_var = ['', '']
    for i in [0,1]:
        if(binvars[i] == 'PT'):
            bin_var[i] = args.pt_var
        elif(binvars[i] == 'ETA'):
            bin_var[i] = args.eta_var
        elif(binvars[i] == 'NSPDHits'):
            bin_var[i] = args.nspdhits_var
        elif(binvars[i] == 'NPVs'):
            bin_var[i] = args.npvs_var
        elif(binvars[i] == 'NTracks'):
            bin_var[i] = args.ntracks_var


    # Silent
    ROOT.gROOT.SetBatch(True)
    # Open and check reference file
    if not os.path.exists(file_name):
        print "Cannot find the file with the reference tree -> %s" % file_name
        sys.exit(1)
    ref_file = ROOT.TFile(file_name, 'r')
    ref_tree = ref_file.Get(tree_name)
    if not ref_tree:
        print "The reference file does not contain the specified tree -> %s" % tree_name
        sys.exit(1)
    if not os.path.exists(file_name):
        print "Cannot find the file with the reference tree -> %s" % file_name
        sys.exit(1)
    # Load binning
    try:
        bins_pt, bins_eta = load_binning_from_txt(bin_file, binvars)
    except OSError:
        print "Cannot load binning scheme from %s" % bin_file
        sys.exit(1)
    # Run
    table = get_efficiency_table(ref_tree, args.stripping_version, args.isphoton_cut, args.isnoth_cut, args.isnote_cut, bins_pt, bins_eta,
                                 isphoton_var=args.isphoton_var,
                                 isnoth_var=args.isnoth_var,
                                 isnote_var=args.isnote_var,
                                 main_var=args.IsNotX,
                                 weight_var=args.weight_var,
                                 pt_var=bin_var[0],
                                 eta_var=bin_var[1])
    # Default output
    if not args.output:
        args.output = ['text']
    # Write outputs
    file_name = '%s_eff_isNotphoton%s_isNotH%s_isNotE%s' % (args.IsNotX, args.isphoton_cut, args.isnoth_cut, args.isnote_cut)
    for output in args.output:
        if output == 'text':
            table.to_txt(binvars, file_name + '.txt')
            print "Written", file_name + '.txt'
        elif output == 'python':
            table.to_python(file_name +'.py')
            print "Written", file_name + '.py'
        elif output == 'cpp':
            table.to_cpp(file_name + '.cpp')
            print "Written", file_name + '.cpp'

# EOF
