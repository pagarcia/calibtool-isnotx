#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   __init__.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   18.11.2014
# =============================================================================
"""Module to handle the creation and usage of calibration table for the
pi0/gamma separation variable."""


# EOF
