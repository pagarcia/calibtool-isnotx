#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   calibration_table.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   18.11.2014
# =============================================================================
"""Write and read calibration tables for the pi0/gamma separation variable."""

import sys
import os
import re


class CalibrationTable(object):
    """Class to store and manipulate calibration tables."""
    def __init__(self, efficiencies):
        """Initialize the calibration table with the efficiencies dictionary.

        :param efficiencies: Efficiencies in bins of pt.
        :type efficiencies: dict

        """
        self._effs = efficiencies

    def get(self):
        """Get the the full efficiency table.

        :rtype: dict

        """
        return self._effs

    def get_points(self):
        """Get the efficiency table as a list of sorted data points

        :rtype: list

        """
        output = []
        for bin_ in sorted(self._effs.keys()):
            output.append((bin_, self._effs[bin_]))
        return output

    def get_efficiency(self, pt, eta):
        """Get the efficiency for a particle with the given pt.

        :param pt: Transverse momentum of the particle.
        :type pt: float

        :returns: Value of the efficiency according to the table.
        :type: tuple of (eff, error)

        :raises: ValueError: If the particle pt is outside the bin boundaries.

        """
        for low_pt, up_pt, low_eta, up_eta in self._effs.keys():
            if pt >= low_pt and pt < up_pt:
                if eta >= low_eta and eta < up_eta:
                    return self._effs[(low_pt, up_pt, low_eta, up_eta)]
        raise ValueError("Out of bounds pt value -> %s or eta value -> %s" % (pt, eta))

    def to_txt(self, binvars, file_name=None):
        """Dump table in txt format.

        If a file name is given, the table is not dumped to screen,
        but to that file.

        :param file_name: Output file.
        :type file_name: str

        """
        format_ = '%s %25s %40s'
        if file_name is None:
            file_ = sys.stdout
        else:
            file_ = open(file_name, 'w')
        print >>file_, '#'*99
        bin1 = binvars[0]
        bin2 = binvars[1]
        print >>file_, format_ % ('#', bin1 + '&&'+ bin2, 'Eff')
        print >>file_, '#'*99
        for edges in sorted(self._effs.keys()):
            bin_str = '[%s, %s, %s, %s)' % edges
            eff_str = '%s +/- %s' % self._effs[edges]
            print >>file_, format_ % (' ', bin_str, eff_str)
        print >>file_, '#'*68
        if file_name:
            file_.close()

    @staticmethod
    def from_txt(file_name):
        """Load table from txt dump.

        :param file_name: Input file.
        :type file_name: str

        :returns: Calibration table.
        :rtype: CalibrationTable

        :raises: OSError: If the input file doesn't exist.
        :raises: ValueError: If the data cannot be decoded.

        """
        txt_regex = re.compile(r'\[([0-9.]*), ([0-9.]*), ([0-9.]*), ([0-9.]*)\)\s*([0-9.]*) \+/- ([0-9.]*)')
        if not os.path.exists(file_name):
            raise OSError("Cannot find input file -> %s" % file_name)
        effs = {}
        with open(file_name) as file_:
            for line in file_:
                if line.startswith('#'):
                    continue
                match = txt_regex.search(line)
                if match:
                    try:
                        low_pt, high_pt, low_eta, high_eta, eff, eff_error = [float(val)
                                                               for val in match.groups()]
                    except:
                        raise ValueError("Cannot decode txt file")
                    effs[(low_pt, high_pt, low_eta, high_eta)] = (eff, eff_error)
        return CalibrationTable(effs)

    def to_python(self, file_name=None):
        """Dump table in python dict format.

        If a file name is given, the table is not dumped to screen,
        but to that file.

        :param file_name: Output file.
        :type file_name: str

        """
        from pprint import pprint
        file_ = None
        if file_name:
            file_ = open(file_name, 'w')
        pprint(self._effs, stream=file_)
        if file_:
            file_.close()

    def to_cpp(self, file_name=None):
        """Dump table in C++ format.

        The C++ code for a function returning the efficiencies for a given
        pt is printed to screen. If a file name is given, the table is not
        dumped to screen, but to that file.

        :param file_name: Output file.
        :type file_name: str

        """
        if file_name is None:
            file_ = sys.stdout
        else:
            file_ = open(file_name, 'w')
        output = "std::pair<double, double> isPhotonEff(double pt, double eta){\n"
        for low_pt, high_pt, low_eta, high_eta in sorted(self._effs.keys()):
            eff, eff_err = self._effs[(low_pt, high_pt, low_eta, high_eta)]
            output += "    if ( pt >= %s && pt < %s && eta>=%s && eta<%s) return std::make_pair(%s, %s) ;\n" % (low_pt,
                                                                                            high_pt,
                                                                                            low_eta,
                                                                                            high_eta,
                                                                                            eff, eff_err)
        output += '}'
        print >>file_, output
        if file_name:
            file_.close()

# EOF
