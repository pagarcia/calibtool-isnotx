#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   add_weight_to_ntuple.py
# @author Clara Remon Alepuz (cremonal@cern.ch)
# @based  on  Miriam Calvo code
# @date   28.03.2020
# =============================================================================
"""Make efficiency plots from calibration samples."""

import os
import sys

import ROOT
from array import array

from IsNotXCalib.utils import pairwise, get_colors, get_weighted_entries, efficiency
from IsNotXCalib.binning import load_binning_from_txt
from IsNotXCalib.defaults import apply_defaults, add_file_info
from IsNotXCalib.calibration.build_efficiency_tables import get_efficiency_table
from IsNotXCalib.calibration.calibration_table import CalibrationTable


def add_weight_var(tree, eff_table, new_var, bin_var_pt, bin_var_eta, MinPT, MaxPT, MinEta, MaxEta):
    """Add resampled variable to the original tree.

    :param tree: Tree to add the variable to.
    :type tree: ROOT.TTree
    :param new_var: Name of the resampled variable.
    :type new_var: str
    :returns: Number of entries in the tree.
    :rtype: int

    """
    # Create new branch
    new_var_val = array('f', [0.])
    new_branch = tree.Branch(new_var, new_var_val, new_var+'/F')
    new_var_err_val = array('f', [0.])
    new_branch2 = tree.Branch(new_var+'_err', new_var_err_val, new_var+'_err/F')

    # Loop and fill
    n_entry = 0
    while tree.GetEntry(n_entry):
        if n_entry % 10000 == 0:
            print 'Reading entry', n_entry
        n_entry += 1
        bin_var_pt_val = getattr(tree, bin_var_pt)
        bin_var_eta_val = getattr(tree, bin_var_eta)

        if bin_var_pt_val< MinPT:
            bin_var_pt_val = MinPT
        if bin_var_pt_val> MaxPT:
            bin_var_pt_val = MaxPT
        if bin_var_eta_val < MinEta:
            bin_var_eta_val = MinEta
        if  bin_var_eta_val > MaxEta:
             bin_var_eta_val = MaxEta

        assigned_value, error = table.get_efficiency(bin_var_pt_val, bin_var_eta_val)
        if str(assigned_value) == 'nan':
            assigned_value = 0.0
            error = 0.0
        new_var_val[0] = assigned_value
        new_var_err_val[0] = error
        # Fill the branch
        new_branch.Fill()
        new_branch2.Fill()
    return n_entry


if __name__ == '__main__':
    import argparse
    # Define parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--binfile', default='', type=str,
                        help='Location of the binning file')
    parser.add_argument('-e', '--energy_range', type=str, default = 'range1', dest='energy_range',
                        help='Energy range', choices = ['range1', 'range2', 'range3', 'range4', 'range5', 'user','range6', 'range7', 'range8_ptmin', 'range8_ptmax','range9'])
    parser.add_argument('--stripping', type=str, default='S21', dest='stripping_version',
                        help='Stripping version of the calibration samples', choices = ['S20', 'S21', 'TURCAL', 'S24', 'S28', 'S29', 'S34'])
    parser.add_argument('--var_eff', type=str, default='IsNotH', dest='IsNotX',
                                                                            help='Variable to compute the efficiencies', choices = ['IsNotH', 'IsNotE'])
    parser.add_argument('--isnothcut', type=float, default = 0.0, dest='isnoth_cut',
                                                    help='Cuts to apply to the IsNotH variable')
    parser.add_argument('--isnotecut', type=float, default = 0.0, dest='isnote_cut',
                                                    help='Cuts to apply to the IsNotE variable')
    parser.add_argument('--isphotoncut', type=float, default=0.0, dest='isphoton_cut',
                        help='Cut to apply to the isPhoton variable')
    parser.add_argument('MCfilename', type=str, help='MC file to add weight')
    parser.add_argument('MCtreename', type=str, help='Name of the MC tree where weight will be added')
    parser.add_argument('MCvarPT', type=str, help='Variable to bin in for transverse momentum')
    parser.add_argument('MCvarETA', type=str, help='Variable to bin in for pseudorapidity')
    parser.add_argument('--mcweightname', type=str, help='Output variable name', default = 'IsPhoton_weight')

    parser.add_argument('--binvar_1', type=str, default='PT', dest='bin1',
    help='Binning variable 1', choices = ['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks'])
    parser.add_argument('--binvar_2', type=str, default='ETA', dest='bin2',
    help='Binning variable 2', choices = ['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks'])


    # Parse
    args       = parser.parse_args()
    args.usemc = False
    erange    = args.energy_range
    args        = apply_defaults(add_file_info(args, erange), erange, 'calibrate')
    file_name  = args.input_file_name
    tree_name  = args.input_tree_name
    bin_file   = args.binfile

    binvars    = [args.bin1,args.bin2]
    bin_var = ['', '']
    for i in [0,1]:
        if(binvars[i] == 'PT'):
            bin_var[i] = args.pt_var
        elif(binvars[i] == 'ETA'):
            bin_var[i] = args.eta_var
        elif(binvars[i] == 'NSPDHits'):
            bin_var[i] = args.nspdhits_var
        elif(binvars[i] == 'NPVs'):
            bin_var[i] = args.npvs_var
        elif(binvars[i] == 'NTracks'):
            bin_var[i] = args.ntracks_var


    # Open and check calibration file
    if not os.path.exists(file_name):
        print "Cannot find the file with the reference tree -> %s" % file_name
        sys.exit(1)
    ref_file = ROOT.TFile(file_name, 'r')
    ref_tree = ref_file.Get(tree_name)
    if not ref_tree:
        print "The reference file does not contain the specified tree -> %s" % tree_name
        sys.exit(1)
    if not os.path.exists(file_name):
        print "Cannot find the file with the reference tree -> %s" % file_name
        sys.exit(1)
    # Load binning
    try:
        bins_pt, bins_eta = load_binning_from_txt(bin_file, binvars)
    except OSError:
        print "Cannot load binning scheme from %s" % bin_file
        sys.exit(1)

    Nbinspt = len(bins_pt)
    Nbinseta = len(bins_eta)
    MinPT = bins_pt[0]
    MaxPT = bins_pt[Nbinspt-2]
    MinEta = bins_eta[0]
    MaxEta = bins_eta[Nbinseta-2]

    #get efficiency table for the given binning
    table = get_efficiency_table(ref_tree, args.stripping_version, args.isphoton_cut, args.isnoth_cut, args.isnote_cut, bins_pt, bins_eta,
                                 isphoton_var=args.isphoton_var,
                                 isnoth_var=args.isnoth_var,
                                 isnote_var=args.isnote_var,
                                 main_var=args.IsNotX,
                                 weight_var=args.weight_var,
                                 pt_var=bin_var[0],
                                 eta_var=bin_var[0])

    # Read file to re-sample
    mc_file_name = args.MCfilename
    mc_tree_name = args.MCtreename
    mc_weight = args.mcweightname
    mc_pt_var = args.MCvarPT
    mc_eta_var = args.MCvarETA


    file_to_add_weights = ROOT.TFile(mc_file_name, 'UPDATE')
    tree = file_to_add_weights.Get(mc_tree_name)
    if not tree:
        raise KeyError("Cannot load tree to resample -> %s" % mc_tree_name)
    # Resample
    _ = add_weight_var(tree, table, mc_weight, mc_pt_var, mc_eta_var, MinPT, MaxPT, MinEta, MaxEta)

    # Close everything
    file_to_add_weights.Write()
    file_to_add_weights.Close()


# EOF
