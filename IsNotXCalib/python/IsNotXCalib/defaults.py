#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   defaults.py
# @author Clara Remon Alepuz (cremonal@cern.ch)
# @based  on Albert Puig code
# @date   28.03.2020
# =============================================================================
"""Store defaults for configuring the resampling tools."""

import os

__calib_samples_folder = '/eos/lhcb/wg/CaloObj/CalibSamples'




__file_info__ = {'S21': {

                    'range1': {'data': {'file_name': 'S21/Bs2JpsiEta_S21Data_2012_sWeights.root',
                                                                     'tree_name': 'Tree',
                                                                     'isphoton_var': 'gamma_isPhoton',
                                                                     'isnoth_var': 'gamma_isNotH',
                                                                     'isnote_var': 'gamma_isNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NSPDHits': 'nSPDHits',
                                                                     'NTracks': 'nTracks',
                                                                     'NPVs': 'nPVs',
                                                                     'weight_var': 'nSigWeight'},
                                                    'mc': {'file_name': 'S21/Bs2JpsiEta_S21MC_2012.root',
                                                                   'tree_name': 'Tree',
                                                                   'isphoton_var': 'gamma_isPhoton',
                                                                   'isnoth_var': 'gamma_isNotH',
                                                                   'isnote_var': 'gamma_isNotE',
                                                                   'PT': 'gamma_PT',
                                                                   'ETA': 'gamma_ETA',
                                                                   'NSPDHits': 'nSPDHits',
                                                                   'NTracks': 'nTracks',
                                                                   'NPVs': 'nPVs',
                                                                   'weight_var': '1'}
                                 },
                    'range2': {'data': {'file_name': 'S21/Bu2JpsiKstr_S21Data_Run1_sWeights.root',
                                                                     'tree_name': 'Tree',
                                                                     'isphoton_var': 'gamma_PP_IsPhoton',
                                                                     'isnoth_var': 'gamma_PP_IsNotH',
                                                                     'isnote_var': 'gamma_PP_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NSPDHits': 'nSPDHits',
                                                                     'NTracks': 'nTracks',
                                                                     'NPVs': 'nPVs',
                                                                     'weight_var': 'nSigWeight'},
                                                    'mc': {'file_name': 'S21/Bu2JpsiKstr_S21MC_Run1.root',
                                                                   'tree_name': 'Tree',
                                                                   'isphoton_var': 'gamma_PP_IsPhoton',
                                                                   'isnoth_var': 'gamma_PP_IsNotH',
                                                                   'isnote_var': 'gamma_PP_IsNotE',
                                                                   'PT': 'gamma_PT',
                                                                   'ETA': 'gamma_ETA',
                                                                   'NSPDHits': 'nSPDHits',
                                                                   'NTracks': 'nTracks',
                                                                   'NPVs': 'nPVs',
                                                                   'weight_var': '1'}
                                 },

                    'range3': {'data': {'file_name': 'S21/Ds2EtapPi_S21Data_2012Down_sWeights.root',
                                                                     'tree_name': 'Tree',
                                                                     'isphoton_var': 'gamma_PP_IsPhoton',
                                                                     'isnoth_var': 'gamma_PP_IsNotH',
                                                                     'isnote_var': 'gamma_PP_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NSPDHits': 'nSPDHits',
                                                                     'NTracks': 'nTracks',
                                                                     'NPVs': 'nPVs',
                                                                     'weight_var': 'nSigWeight'
                                                                     }
                                 },

                    'range4': {'data': {'file_name': 'S21/Dsstr2DsGamma_S21Data_2012_sWeights.root',
                                            'tree_name': 'Tree',
                                            'isphoton_var': 'gamma_PP_IsPhoton',
                                            'isnoth_var': 'gamma_PP_IsNotH',
                                            'isnote_var': 'gamma_PP_IsNotE',
                                            'PT': 'gamma_PT',
                                            'ETA': 'gamma_ETA',
                                            'NSPDHits': 'nSPDHits',
                                            'NTracks': 'nTracks',
                                            'NPVs': 'nPVs',
                                            'weight_var': 'nSigWeight'
                                            }
                                   },
                    'range5': {'data': {'file_name': 'S21/KstGamma_S21Data_sWeights.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'gamma_PP_IsPhoton',
                                                                     'isnoth_var': 'gamma_PP_IsNotH',
                                                                     'isnote_var': 'gamma_PP_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NSPDHits': 'nSPDHits',
                                                                     'NTracks': 'nTracks',
                                                                     'NPVs': 'nPVs',
                                                                     'weight_var': 'nkstgWeight'},
                                                            'mc': {'file_name': 'S21/KstGamma_S21MC_2012.root',
                                                                   'tree_name': 'DecayTree',
                                                                   'isphoton_var': 'gamma_PP_IsPhoton',
                                                                   'isnoth_var': 'gamma_PP_IsNotH',
                                                                   'isnote_var': 'gamma_PP_IsNotE',
                                                                   'PT': 'gamma_PT',
                                                                   'ETA': 'gamma_ETA',
                                                                   'NSPDHits': 'nSPDHits',
                                                                   'NTracks': 'nTracks',
                                                                   'NPVs': 'nPVs',
                                                                   'weight_var': '1'}
                                 }
                         },

                 'TURCAL': {

                    'range9': {'data': {'file_name': '/afs/cern.ch/work/p/pagarcia/public/tree_turcal_Dsst2DsGamma.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'gamma_IsPhoton',
                                                                     'isnoth_var': 'gamma_IsNotH',
                                                                     'isnote_var': 'gamma_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                      'NTracks': 'nTracks',
                                                                     'weight_var': 'sWeight'}
},

                    'range3': {'data': {'file_name': '/afs/cern.ch/work/p/pagarcia/public/tree_turcal_Ds2EtapPi.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'gamma_IsPhoton',
                                                                     'isnoth_var': 'gamma_IsNotH',
                                                                     'isnote_var': 'gamma_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NTracks': 'nTracks',
                                                                     'weight_var': 'sWeight'}
    },

                       'range7': {'data': {'file_name': '/afs/cern.ch/work/p/pagarcia/public/tree_turcal_Dst2D0piM.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'pi0_IsPhoton',
                                                                     'isnoth_var': 'pi0_IsNotH',
                                                                     'isnote_var': 'pi0_IsNotE',
                                                                     'PT': 'pi0_PT',
                                                                     'ETA': 'pi0_ETA',
                                                                     'NTracks': 'nTracks',
                                                                     'weight_var': 'sWeight'}
    },

                       'range8_ptmin': {'data': {'file_name': '/afs/cern.ch/work/p/pagarcia/public/tree_turcal_Dst2D0piR.root',
                                                                     'tree_name': 'Tree',
                                                                     'isphoton_var': 'gamma_ptmin_IsPhoton',
                                                                     'isnoth_var': 'gamma_ptmin_IsNotH',
                                                                     'isnote_var': 'gamma_ptmin_IsNotE',
                                                                     'PT': 'gamma_ptmin_PT',
                                                                     'ETA': 'gamma_ptmin_ETA',
                                                                     'NTracks': 'nTracks',
                                                                     'weight_var': 'sWeight'}
    },


                     'range8_ptmax': {'data': {'file_name': '/afs/cern.ch/work/p/pagarcia/public/tree_turcal_Dst2D0piR.root',
                                                                     'tree_name': 'Tree',
                                                                     'isphoton_var': 'gamma_ptmax_IsPhoton',
                                                                     'isnoth_var': 'gamma_ptmax_IsNotH',
                                                                     'isnote_var': 'gamma_ptmax_IsNotE',
                                                                     'PT': 'gamma_ptmax_PT',
                                                                     'ETA': 'gamma_ptmax_ETA',
                                                                     'NTracks': 'nTracks',
                                                                     'weight_var': 'sWeight'}
    },

                         'range6': {'data': {'file_name': '/afs/cern.ch/work/p/pagarcia/public/tree_turcal_Eta2MuMuGamma.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'gamma_IsPhoton',
                                                                     'isnoth_var': 'gamma_IsNotH',
                                                                     'isnote_var': 'gamma_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NTracks': 'nTracks',
                                                                     'weight_var': 'sWeight'}
    }
}, 

'S29': {

                    'range5': {'data': {'file_name': '/afs/cern.ch/user/c/chefdevi/eoscaloobj/CalibSamples/Validation/Fit2/AddEta/KstGamma_S29R2Data_sWeights.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'gamma_PP_IsPhoton',
                                                                     'isnoth_var': 'gamma_PP_IsNotH',
                                                                     'isnote_var': 'gamma_PP_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NSPDHits': 'nSPDHits',
                                                                     'NTracks': 'nTracks',
                                                                     'NPVs': 'nPVs',
                                                                     'weight_var': 'nSigWeight'},
                                                    'mc': {'file_name': '/afs/cern.ch/user/c/chefdevi/eoscaloobj/CalibSamples/Validation/Fit2/AddEta/KstGamma_S29R2MC.root',
                                                                   'tree_name': 'DecayTree',
                                                                   'isphoton_var': 'gamma_PP_IsPhoton',
                                                                   'isnoth_var': 'gamma_PP_IsNotH',
                                                                   'isnote_var': 'gamma_PP_IsNotE',
                                                                   'PT': 'gamma_PT',
                                                                   'ETA': 'gamma_ETA',
                                                                   'NSPDHits': 'nSPDHits',
                                                                   'NTracks': 'nTracks',
                                                                   'NPVs': 'nPVs',
                                                                   'weight_var': '1'}
                                 }



                 },
'S34': {

                    'range5': {'data': {'file_name': '/afs/cern.ch/user/c/chefdevi/eoscaloobj/CalibSamples/Validation/Fit2/AddEta/KstGamma_S34Data_sWeights.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'gamma_PP_IsPhoton',
                                                                     'isnoth_var': 'gamma_PP_IsNotH',
                                                                     'isnote_var': 'gamma_PP_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NSPDHits': 'nSPDHits',
                                                                     'NTracks': 'nTracks',
                                                                     'NPVs': 'nPVs',
                                                                     'weight_var': 'nSigWeight'},
                                                    'mc': {'file_name': '/afs/cern.ch/user/c/chefdevi/eoscaloobj/CalibSamples/Validation/Fit2/KstGamma_S34MC.root',
                                                                   'tree_name': 'DecayTree',
                                                                   'isphoton_var': 'gamma_PP_IsPhoton',
                                                                   'isnoth_var': 'gamma_PP_IsNotH',
                                                                   'isnote_var': 'gamma_PP_IsNotE',
                                                                   'PT': 'gamma_PT',
                                                                   'ETA': 'gamma_ETA',
                                                                   'NSPDHits': 'nSPDHits',
                                                                   'NTracks': 'nTracks',
                                                                   'NPVs': 'nPVs',
                                                                   'weight_var': '1'}
                                 }



                 },


                 'S24': {

                   'range5': {'data': {'file_name': '/afs/cern.ch/user/c/chefdevi/eoscaloobj/CalibSamples/Validation/Fit2/AddEta/KstGamma_S24R2Data_sWeights.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'gamma_PP_IsPhoton',
                                                                     'isnoth_var': 'gamma_PP_IsNotH',
                                                                     'isnote_var': 'gamma_PP_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NSPDHits': 'nSPDHits',
                                                                     'NTracks': 'nTracks',
                                                                     'NPVs': 'nPVs',
                                                                     'weight_var': 'nSigWeight'}

                                 }



                 },

'S28': {

                   'range5': {'data': {'file_name': '/afs/cern.ch/user/c/chefdevi/eoscaloobj/CalibSamples/Validation/Fit2/AddEta/KstGamma_S28R2Data_sWeights.root',
                                                                     'tree_name': 'DecayTree',
                                                                     'isphoton_var': 'gamma_PP_IsPhoton',
                                                                     'isnoth_var': 'gamma_PP_IsNotH',
                                                                     'isnote_var': 'gamma_PP_IsNotE',
                                                                     'PT': 'gamma_PT',
                                                                     'ETA': 'gamma_ETA',
                                                                     'NSPDHits': 'nSPDHits',
                                                                     'NTracks': 'nTracks',
                                                                     'NPVs': 'nPVs',
                                                                     'weight_var': 'nSigWeight'}

                                 }



                 }
                 }


__defaults__ = {'S21': {
                        'range1': {'make': {'var': 'gamma_isPhoton',
                                                                    'varh': 'gamma_isNotH',
                                                                    'vare': 'gamma_isNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range1.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range1.txt',
                                                                        'calib_sample': 'Bs #rightarrow J/#Psi #eta'}
                                                          },

                        'range2': {'make': {'var': 'gamma_PP_IsPhoton',
                                                                    'varh': 'gamma_PP_IsNotH',
                                                                    'vare': 'gamma_PP_IsNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range2.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range2.txt',
                                                                        'calib_sample': 'Bu #rightarrow J/#Psi K^{*}'}
                                                          },

                        'range3': {'make': {'var': 'gamma_PP_IsPhoton',
                                                                    'varh': 'gamma_PP_IsNotH',
                                                                    'vare': 'gamma_PP_IsNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range3.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range3.txt',
                                                                        'calib_sample': 'D_{s}^{*} #rightarrow #eta #pi'}
                                                          },

                        'range4': {'make': {'var': 'gamma_PP_IsPhoton',
                                            'varh': 'gamma_PP_IsNotH',
                                            'vare': 'gamma_PP_IsNotE',
                                           'binvarPT': 'gamma_PT',
                                           'binvarETA': 'gamma_ETA',
                                           'binfile': '../../../options/binning_resampling_range4.txt',
                                           'outputfile': 'gamma_ref_histos.root'
                                           },
                                  'apply': {'treename': 'DecayTree',
                                            'histosfile': 'gamma_ref_histos.root',
                                            'binvarPT': 'gamma_PT',
                                            'binvarETA': 'gamma_ETA',
                                            'newvar': 'IsNotX_Resampled'},
                                  'calibrate': {'binfile': '../../../options/binning_calibration_range4.txt',
                                                'calib_sample': 'D_{s}^{*} #rightarrow D_{s} #gamma'}
                                  },
                        'range5': {'make': {'var': 'gamma_PP_IsPhoton',
                                              'varh': 'gamma_PP_IsNotH',
                                              'vare': 'gamma_PP_IsNotE',
                                              'binvarPT': 'gamma_PT',
                                              'binvarETA': 'gamma_ETA',
                                              'binfile': '../../../options/binning_resampling_range5.txt',
                                              'outputfile': 'gamma_ref_histos.root'
                                              },
                                     'apply': {'treename': 'DecayTree',
                                              'histosfile': 'gamma_ref_histos.root',
                                              'binvarPT': 'gamma_PT',
                                              'binvarETA': 'gamma_ETA',
                                              'newvar': 'IsNotX_Resampled'},
                                     'calibrate': {'binfile': '../../../options/binning_calibration_range5.txt',
                                                   'calib_sample': 'B^{0} #rightarrow K^{*0}(#rightarrow K^{+}#pi^{-})#gamma'}
                                                          }
                        ,
              'user': {'make': {'var': 'gamma_CaloHypo_isPhoton',
                                                    'varh': 'gamma_PP_IsNotH',
                                                    'vare': 'gamma_PP_IsNotE',
                                                    'binvarPT': 'gamma_PT',
                                                    'binvarETA': 'gamma_ETA',
                                                    'binfile': '../../../options/binning_resampling_range5.txt',
                                                    'outputfile': 'gamma_ref_histos.root',
                                                    'cutCL': 0.0},
                                            'apply': {'treename': 'DecayTree',
                                                      'histosfile': 'gamma_ref_histos.root',
                                                      'binvarPT': 'gamma_PT',
                                                      'binvarETA': 'gamma_ETA',
                                                      'newvar': 'gamma_IsNotX_Resampled'},
                                            'calibrate': {'binfile': '../../../options/binning_calibration_range5.txt',
                                                          'calib_sample': 'B^{0} #rightarrow K^{*0}(#rightarrow K^{+}#pi^{-})#gamma'}
                                                          }},

'TURCAL': {
                        'range9': {'make': {'var': 'gamma_isPhoton',
                                                                    'varh': 'gamma_IsNotH',
                                                                    'vare': 'gamma_IsNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range9.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range9.txt',
                                                                        'calib_sample': 'Bs #rightarrow J/#Psi #eta'}
                                                          },
                        'range3': {'make': {'var': 'gamma_PP_IsPhoton',
                                                                    'varh': 'gamma_IsNotH',
                                                                    'vare': 'gamma_IsNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range3.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range3.txt',
                                                                        'calib_sample': 'Bu #rightarrow J/#Psi K^{*}'}
                                                          },
                        'range7': {'make': {'var': 'pi0_IsPhoton',
                                                                    'varh': 'pi0_IsNotH',
                                                                    'vare': 'pi0_IsNotE',
                                                                   'binvarPT': 'pi0_PT',
                                                                   'binvarETA': 'pi0_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range7.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'pi0_PT',
                                                                    'binvarETA': 'pi0_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range7.txt',
                                                                        'calib_sample': 'D_{s}^{*} #rightarrow #eta #pi'}
                                                          },

                        'range8_ptmin': {'make': {'var': 'gamma_ptmin_IsPhoton',
                                            'varh': 'gamma_ptmin_IsNotH',
                                            'vare': 'gamma_ptmin_IsNotE',
                                           'binvarPT': 'gamma_ptmin_PT',
                                           'binvarETA': 'gamma_ptmin_ETA',
                                           'binfile': '../../../options/binning_resampling_range8.txt',
                                           'outputfile': 'gamma_ref_histos.root'
                                           },
                                  'apply': {'treename': 'DecayTree',
                                            'histosfile': 'gamma_ref_histos.root',
                                            'binvarPT': 'gamma_ptmin_PT',
                                            'binvarETA': 'gamma_ptmin_ETA',
                                            'newvar': 'IsNotX_Resampled'},
                                  'calibrate': {'binfile': '../../../options/binning_calibration_range8.txt',
                                                'calib_sample': 'D_{s}^{*} #rightarrow D_{s} #gamma'}
                                  },

                       'range8_ptmax': {'make': {'var': 'gamma_ptmax_IsPhoton',
                                            'varh': 'gamma_ptmax_IsNotH',
                                            'vare': 'gamma_ptmax_IsNotE',
                                           'binvarPT': 'gamma_ptmax_PT',
                                           'binvarETA': 'gamma_ptmax_ETA',
                                           'binfile': '../../../options/binning_resampling_range8.txt',
                                           'outputfile': 'gamma_ref_histos.root'
                                           },
                                  'apply': {'treename': 'DecayTree',
                                            'histosfile': 'gamma_ref_histos.root',
                                            'binvarPT': 'gamma_ptmax_PT',
                                            'binvarETA': 'gamma_ptmax_ETA',
                                            'newvar': 'IsNotX_Resampled'},
                                  'calibrate': {'binfile': '../../../options/binning_calibration_range8.txt',
                                                'calib_sample': 'D_{s}^{*} #rightarrow D_{s} #gamma'}
                                  },

                        'range6': {'make': {'var': 'gamma_IsPhoton',
                                              'varh': 'gamma_IsNotH',
                                              'vare': 'gamma_IsNotE',
                                              'binvarPT': 'gamma_PT',
                                              'binvarETA': 'gamma_ETA',
                                              'binfile': '../../../options/binning_resampling_range6.txt',
                                              'outputfile': 'gamma_ref_histos.root'
                                              },
                                   'apply': {'treename': 'DecayTree',
                                              'histosfile': 'gamma_ref_histos.root',
                                              'binvarPT': 'gamma_PT',
                                              'binvarETA': 'gamma_ETA',
                                              'newvar': 'IsNotX_Resampled'},
                                     'calibrate': {'binfile': '../../../options/binning_calibration_range6.txt',
                                                   'calib_sample': 'B^{0} #rightarrow K^{*0}(#rightarrow K^{+}#pi^{-})#gamma'}
                                                          }

},
'S29': {
                        'range5': {'make': {'var': 'gamma_PP_IsPhoton',
                                                                    'varh': 'gamma_PP_IsNotH',
                                                                    'vare': 'gamma_PP_IsNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range5.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range5.txt',
                                                                        'calib_sample': 'Bs #rightarrow J/#Psi #eta'}
                                                          }

                },
'S28': {
                        'range5': {'make': {'var': 'gamma_PP_IsPhoton',
                                                                    'varh': 'gamma_PP_IsNotH',
                                                                    'vare': 'gamma_PP_IsNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range5.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range5.txt',
                                                                        'calib_sample': 'Bs #rightarrow J/#Psi #eta'}
                                                          }

                },

'S34': {
                        'range5': {'make': {'var': 'gamma_PP_IsPhoton',
                                                                    'varh': 'gamma_PP_IsNotH',
                                                                    'vare': 'gamma_PP_IsNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range5.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range5.txt',
                                                                        'calib_sample': 'Bs #rightarrow J/#Psi #eta'}
                                                          }

                },

'S24': {
                        'range5': {'make': {'var': 'gamma_PP_IsPhoton',
                                                                    'varh': 'gamma_PP_IsNotH',
                                                                    'vare': 'gamma_PP_IsNotE',
                                                                   'binvarPT': 'gamma_PT',
                                                                   'binvarETA': 'gamma_ETA',
                                                                   'binfile': '../../../options/binning_resampling_range5.txt',
                                                                   'outputfile': 'gamma_ref_histos.root'
                                                                   },
                                                          'apply': {'treename': 'DecayTree',
                                                                    'histosfile': 'gamma_ref_histos.root',
                                                                    'binvarPT': 'gamma_PT',
                                                                    'binvarETA': 'gamma_ETA',
                                                                    'newvar': 'IsNotX_Resampled'},
                                                          'calibrate': {'binfile': '../../../options/binning_calibration_range5.txt',
                                                                        'calib_sample': 'Bs #rightarrow J/#Psi #eta'}
                                                          }

                }

}


def add_file_info(args, erange):

    """Add calibration file information to parsed args.

    :params args: Parsed arguments.
    :type args: argparse.Namespace
    :param particle: Type of particle to get the defaults from.
    :type particle: str

    :returns: The modified parsed args.
    :rtype: argparse.Namespace

    """
    # Load file information
    Str = args.stripping_version
    Erange = args.energy_range
    data_type = 'data' if not args.usemc else 'mc'

    if(Erange == 'user'):
        import json
        sampleinfo = open('../calibrationsample_info.txt', 'r')
        sampleinfo_dict = json.loads(sampleinfo.read())
        file_info = sampleinfo_dict[data_type]
        args.input_file_name = str(file_info['file_name'])

    else:
        file_info = __file_info__[Str][erange][data_type]
        args.input_file_name = str(os.path.join(__calib_samples_folder, file_info['file_name']))


    args.input_file_name = str(os.path.join(__calib_samples_folder, file_info['file_name']))
    args.input_tree_name = str(file_info['tree_name'])
    args.isphoton_var  = str(file_info['isphoton_var'])
    args.isnoth_var = str(file_info['isnoth_var'])
    args.isnote_var = str(file_info['isnote_var'])

    binning_vars = [args.bin1, args.bin2]

    for binning_var in binning_vars:
        if(binning_var == 'PT'):
            if not file_info[binning_var]:
                raise ValueError("Binning variable %s not not available for the %s calibration sample" % (binning_var, Str))
            else:
                args.pt_var = str(file_info[binning_var])

        elif(binning_var == 'ETA'):
            if not file_info[binning_var]:
                raise ValueError("Binning variable %s not not available for the %s calibration sample" % (binning_var, Str))
            else:
                args.eta_var = str(file_info[binning_var])
        elif(binning_var == 'NSPDHits'):
            if not file_info[binning_var]:
                raise ValueError("Binning variable %s not not available for the %s calibration sample" % (binning_var, Str))
            else:
                args.nspdhits_var = str(file_info[binning_var])
        elif(binning_var == 'NPVs'):
            if not file_info[binning_var]:
                raise ValueError("Binning variable %s not not available for the %s calibration sample" % (binning_var, Str))
            else:
                args.npvs_var = str(file_info[binning_var])
        elif(binning_var == 'NTracks'):
            if not file_info[binning_var]:
                raise ValueError("Binning variable %s not not available for the %s calibration sample" % (binning_var, Str))
            else:
                args.ntracks_var = str(file_info[binning_var])

    args.weight_var = str(file_info['weight_var'])
    return args


def apply_defaults(args, erange, action):
    """Get value of the parser variable.

    Defaults for the given particle/action combination are applied in
    case the value of the parsed args is ''.

    :params args: Parsed arguments.
    :type args: argparse.Namespace
    :param particle: Type of particle to get the defaults from.
    :type particle: str
    :param action: Action to get the defaults for
    :type action: str

    :returns: The modified parsed args.
    :rtype: argparse.Namespace

    :raises: ValueError: If the energy range or action are not allowed.

    """
    stripping = args.stripping_version
    print 'Using defaults for', stripping
    if stripping not in __defaults__.keys():
        raise ValueError("Samples not available for range %s" % stripping)


    if erange not in __defaults__[stripping]:
        print "ENRGY RANGES",  __defaults__[stripping]
        raise ValueError("Unknown pt range -> %s" % erange)
    default_values = __defaults__[stripping][erange][action]
    if action not in __defaults__[stripping][erange]:
        raise ValueError("Unknown action for energy range '%s' -> %s" % (erange, action))


    # Apply defaults
    for var_name, val in default_values.items():
        if not hasattr(args, var_name) or not getattr(args, var_name):
            setattr(args, var_name, val)

    return args

# EOF
