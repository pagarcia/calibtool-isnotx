#!/usr/bin/env python
# =============================================================================
# @file   binning.py
# @author Albert Puig (albert.puig@epfl.ch)
# @date   24.06.2014
# =============================================================================
"""Binning-related utilities."""
from __future__ import with_statement
import os


def load_binning_from_txt(file_name, binvars):
    """Load binning from a text file.

    Each line should contain lower_edge upper_edge pairs.

    :params file_name: File to load bins from.
    :type file_name: str

    :returns: List of bins.
    :rtype: list

    :raises: OSError: If bin file doesn't exist.

    """
    bin1, bin2 = binvars[0], binvars[1]
    if not os.path.exists(file_name):
        raise OSError("Binning file does not exist -> %s" % file_name)
    bins_pt = []
    bins_eta = []
    all_lines_from_file = []
    with open(file_name) as file_:
        for line in file_:
            if not line:
                continue
            if not line.startswith('-'):
                all_lines_from_file.append(float(line.rstrip('\n')))
            else:
                all_lines_from_file.append(str(line.rstrip('\n')))

    for i in range(len(all_lines_from_file)):
        linetype = type(all_lines_from_file[i])
        if (linetype==float):
            continue
        else:
            if(all_lines_from_file[i].startswith('- '+ bin1)):
                for k in range(len(all_lines_from_file)):
                    limit = k+1+i
                    if(limit == len(all_lines_from_file)):
                        break
                    bin2store = all_lines_from_file[i+k+1]
                    if not type(bin2store)==str:
                        bins_pt.append(bin2store)
                        continue
                    else:
                        break
            elif (all_lines_from_file[i].startswith('- '+ bin2)):
                for j in range(len(all_lines_from_file)):
                    limit = j+1+i
                    if(limit == len(all_lines_from_file)):
                        break
                    bin2store = all_lines_from_file[i+j+1]
                    if not type(bin2store)==str:
                        bins_eta.append(bin2store)
                        continue
                    else:
                        break
    print "Binning in ", bin1, " :", bins_pt
    print "Binning in ", bin2, " :", bins_eta

    return bins_pt, bins_eta

def load_binning_from_root(root_file):
    """Load binning from preexisting ROOT file.

    It uses the histogram names created in make_histos to guess the
    binning used in their creation.

    :param root_file: ROOT file containing calibration histos.
    :type root_file: ROOT.TFile

    :returns: List of bins.
    :rtype: list

    """
    import re
    bin_re = re.compile(r'\[([0-9]*\.[0-9][0-9]),([0-9]*\.[0-9][0-9])]')
    bins = []
    for key in root_file.GetListOfKeys():
        match = bin_re.match(key.GetName())
        if match:
            low_bin, high_bin = [float(edge) for edge in match.groups()]
            bins.append(low_bin)
            bins.append(high_bin)
    return list(set(bins))

# EOF
