#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   __init__.py
# @author Clara Remon (cremonal@cern.ch)
# @date   03-2020
# =============================================================================
"""IsNotE and IsNotH separation variable calibration package."""

# EOF
