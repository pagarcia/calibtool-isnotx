{"data": {"file_name": "/afs/cern.ch/work/p/pagarcia/UraniaDev_v7r0/IsNotXCalib/python/IsNotXCalib/resampling/KstGamma_S34Data_sWeights.root",
      "tree_name": "DecayTree",
      "isphoton_var": "gamma_PP_IsPhoton",
      "isnoth_var": "gamma_PP_IsNotH",
      "isnote_var": "gamma_PP_IsNotE",
      "PT": "gamma_PT",
      "ETA": "gamma_ETA",
      "NSPDHits": "NSPDHits",
      "NTracks": "nTracks",
      "NPVs": "nPVs",
      "weight_var": "nSigWeight",
      "cl_var": "gamma_CL"},


"mc": {"file_name":"/afs/cern.ch/work/p/pagarcia/UraniaDev_v7r0/IsNotXCalib/python/IsNotXCalib/resampling/KstGamma_S34MC.root",
      "tree_name": "DecayTree",
      "isphoton_var": "gamma_PP_IsPhoton",
      "isnoth_var": "gamma_PP_IsNotH",
      "isnote_var": "gamma_PP_IsNotE",
      "PT": "gamma_PT",
      "ETA": "gamma_ETA",
      "NSPDHits": "nSPDHits",
      "NTracks": "nTracks",
      "NPVs": "nPVs",
      "weight_var": "sWeight",
      "cl_var": "gamma_CL"}
                                   
}

