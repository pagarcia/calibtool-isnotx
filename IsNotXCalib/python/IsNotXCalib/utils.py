#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   utils.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   13.11.2014
# =============================================================================
"""Utility functions for pi0/gamma separation package."""

from itertools import tee, izip, cycle

import ROOT


def pairwise(iterable):
    """Loop over an iterable and extract pairs of consecutive elements.

    :param iterable: Iterable to loop on.
    :type iterable: iterable object.

    :returns: List of pairs.
    :type: List

    """
    first, second = tee(iterable)
    next(second, None)
    return izip(first, second)


def get_colors():
    """Get a fresh color iterator."""
    return cycle([ROOT.kBlue,
                  ROOT.kBlack,
                  ROOT.kRed,
                  ROOT.kGreen,
                  ROOT.kGray,
                  ROOT.kMagenta,
                  ROOT.kCyan,
                  ROOT.kOrange,
                  ROOT.kSpring,
                  ROOT.kYellow,
                  ROOT.kTeal,
                  ROOT.kViolet,
                  ROOT.kAzure,
                  ROOT.kPink])

def get_weighted_entries(tree, cut, weight):
    """Determine the number of weighted entries in a tree.

    :param tree: Tree to cut entries of.
    :type tree: ROOT.TTree
    :param cut: Cut to apply when counting.
    :type cut: str
    :param weight: Weight to apply.
    :type weight: str

    :returns: Number of weighted events.
    :rtype: float

    """
    hist = ROOT.TH1F("weighted", "weighted", 1, -1, 2)
    full_cut = '(%s) * %s' % (cut, weight)
    tree.SetWeight(1)
    tree.Draw('1>>%s' % hist.GetName(), full_cut)
    entries = hist.Integral()
    hist.IsA().Destructor(hist)
    return entries


def efficiency(n_res, n_trials):
    """Calculate the efficiency of n_res in n_trials.

    Central value and error interval (symmetric) is calculated using the
    Wilson score (see Cousins (2009) [arXiv:0905.3831v2] for details).

    :param n_res: Number of positive outcomes.
    :type n_res: float
    :param n_trials: Number of tries.
    :type n_trials: float

    :returns: Efficiency and its error.
    :rtype: tuple (float, float)

    """
    from math import sqrt
    # Check pathological case
    if n_trials == 0. or n_res<0:
        return (0.0, 0.0)
    # Calculate some values needed all around
    t = 1.0/n_trials
    rho_hat = float(n_res)/float(n_trials)
    # Efficiency
    eff = (rho_hat + t/2.0) / (1.0 + t)
    if rho_hat < 1.0:
        eff_error = sqrt(rho_hat*(1-rho_hat)*t + t*t/4.0)/(1.0 + t)
    else:
        eff_error = sqrt(t*t/4.0)/(1.0 + t)
    #return (eff, eff_error)
    tmp1 = float('%.3f' %eff)
    tmp2 = float('%.3f' %eff_error)
    return (tmp1, tmp2)
# EOF
