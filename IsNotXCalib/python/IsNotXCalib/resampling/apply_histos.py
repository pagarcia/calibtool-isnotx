#!/usr/bin/env python
# =============================================================================
# @file   make_histos.py
# @author Clara Remon Alepuz (cremonal@cern.ch)
# @based  on  Vicente Rives code
# @date   28.03.2020
# =============================================================================
"""Apply reference histograms to resample a MC file."""

import os
import sys
import re
from array import array

import ROOT

from IsNotXCalib.defaults import apply_defaults
from IsNotXCalib.utils import pairwise


def load_reference_histos(root_file):
    """Load reference histos from preexisting ROOT file.

    It also guesses the binning used in their creation by checking
    the histogram names created in make_histos.

    :param root_file: ROOT file containing calibration histos.
    :type root_file: ROOT.TFile

    :returns: Histograms and binning
    :rtype: tuple of (dict, list)

    """

    bins_pt  = []
    bins_eta = []
    histos = {}
    bin_re = re.compile(r'\[([0-9]*\.[0-9][0-9]),([0-9]*\.[0-9][0-9]),([0-9]*\.[0-9][0-9]),([0-9]*\.[0-9][0-9])]')
    for key in root_file.GetListOfKeys():
        key_name = key.GetName()
        obj = root_file.Get(key_name)
        if isinstance(obj, ROOT.TH1F):
            match = bin_re.match(key_name)
            if match:
                low_pt_bin, high_pt_bin, low_eta_bin, high_eta_bin = [float(edge) for edge in match.groups()]
                bins_pt.append(low_pt_bin)
                bins_pt.append(high_pt_bin)
                bins_eta.append(low_eta_bin)
                bins_eta.append(high_eta_bin)
                histos[str(low_pt_bin)+'_'+str(low_eta_bin)] = obj
    return histos, list(set(bins_pt)), list(set(bins_eta))


def add_resampled_var(tree, ref_histos, new_var, bin_var_1, bin_var_2, binningPT, binningETA):
    """Add resampled variable to the original tree.

    :param tree: Tree to add the variable to.
    :type tree: ROOT.TTree
    :param ref_histos: Reference histograms for resampling.
    :type ref_histos: dict
    :param new_var: Name of the resampled variable.
    :type new_var: str
    :param binning: Binning scheme for resampling.
    :type binning: dict

    :returns: Number of entries in the tree.
    :rtype: int

    """
    # Create new branch
    new_var_val = array('f', [0.])
    new_branch = tree.Branch(new_var, new_var_val, new_var+'/F')
    # Loop and fill
    n_entry = 0
    while tree.GetEntry(n_entry):
        if n_entry % 10000 == 0:
            print 'Resampling entry', n_entry
        n_entry += 1
        bin_var_pt_val = getattr(tree, bin_var_1)
        bin_var_eta_val = getattr(tree, bin_var_2)
        for low_edge_pt, high_edge_pt in pairwise(binningPT):
            for low_edge_eta, high_edge_eta in pairwise(binningETA):
                # Find the bin the bin variable sits in
                if (bin_var_pt_val >= low_edge_pt) and (bin_var_pt_val < high_edge_pt) and (bin_var_eta_val >= low_edge_eta) and (bin_var_eta_val < high_edge_eta):
                    # Get random number according to reference histogram
                    assigned_value = ref_histos[str(low_edge_pt)+'_'+str(low_edge_eta)].GetRandom()
                    if str(assigned_value) == 'nan':
                        assigned_value = ref_histos[str(low_edge_pt)+'_'+str(low_edge_eta)].GetMean()
                    new_var_val[0] = assigned_value
        # Fill the branch
        new_branch.Fill()
    return n_entry


def resample(file_name, tree_name, bin_var_1, bin_var_2,new_var_name, histos_file_name):
    """Add resampled variable to a ROOT file.

    :param file_name: File to resample.
    :type file_name: str
    :param tree_name: Tree to resample.
    :type tree_name: str
    :param new_var_name: Variable to resample to.
    :type new_var_name: str
    :param histos_file_name: File containing the reference histograms.
    :type histos_file_name: str

    :raises: KeyError: If the tree cannot be loaded.

    """
    # Load reference histos
    if not os.path.exists(histos_file_name):
        raise OSError("Ca")
    histos_file = ROOT.TFile(histos_file_name, 'READ')
    histos, binningPT, binningETA = load_reference_histos(histos_file)
    binningPT.sort()
    binningETA.sort()
    # Read file to re-sample
    file_to_resample = ROOT.TFile(file_name, 'UPDATE')
    tree = file_to_resample.Get(tree_name)
    if not tree:
        raise KeyError("Cannot load tree to resample -> %s" % tree_name)
    # Resample
    _ = add_resampled_var(tree, histos, new_var_name, bin_var_1, bin_var_2, binningPT, binningETA)
    # Close everything")
    file_to_resample.Write()
    file_to_resample.Close()
    histos_file.Close()

if __name__ == '__main__':
    import argparse
    # Define parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--treename', type=str, help='Name of the tree to resample')
    parser.add_argument('--histosfile', type=str, help='File containing the reference histograms')
    parser.add_argument('--bin1_name', type=str, default = 'gamma_PT', help='Name in the tree of the binning variable 1')
    parser.add_argument('--bin2_name', type=str, default = 'gamma_ETA', help='Name in the tree of the binning variable 2')
    parser.add_argument('--newvar', type=str, help='Output variable name')
    parser.add_argument('--filename', type=str, help='File to resample')
    parser.add_argument('-e', '--energy_range', type=str, default = 'high_pt', dest='energy_range',
                            help='Energy range', choices = ['range1', 'range2', 'range3', 'range4', 'range5', 'user', 'range6', 'range7', 'range8_ptmin', 'range8_ptmax','range9'])
    parser.add_argument('--stripping', type=str, default='S21', dest='stripping_version',
                            help='Stripping version of the calibration samples', choices = ['S20', 'S21', 'TURCAL', 'S24', 'S28', 'S29', 'S34'])

    # Parse
    args             = parser.parse_args()
    erange           = args.energy_range
    args             = apply_defaults(args, erange, 'apply')
    file_name        = args.filename
    tree_name        = args.treename
    histos_file_name = args.histosfile
    bin_var_1        = args.bin1_name
    bin_var_2        = args.bin2_name
    new_var          = args.newvar


    # Check files exist
    if not os.path.exists(file_name):
        print "Cannot find file to resample -> %s" % file_name
        sys.exit(1)
    if not os.path.exists(histos_file_name):
        print "Cannot find reference histograms file -> %s" % histos_file_name
        sys.exit(1)
    # Load reference histos
    try:
        resample(file_name, tree_name, bin_var_1, bin_var_2, new_var, histos_file_name)
    except KeyError:
        print "Cannot load tree to resample -> %s" % tree_name
        sys.exit(1)
    # Print and exit
    print "All went well. Variable %s has been added to the tree, enjoy!" % new_var

# EOF
