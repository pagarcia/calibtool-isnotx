#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   __init__.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   13.11.2014
# =============================================================================
"""Resampling scripts for the gamma/pi0 separation tool."""

# EOF
