import os
import ROOT
ROOT.gROOT.SetBatch()
ROOT.gSystem.Load('libRooFit')

from ROOT import gStyle
from math import *
gStyle.SetOptStat(0000000000)

def plotcompare(myvar, data_file, mc_file, weight, treen, name, var, varmc, varresmc):

    f = ROOT.TFile.Open(data_file)
    fmc = ROOT.TFile.Open(mc_file)

    tree = f.Get(treen)
    print "tree", treen
    treemc = fmc.Get(treen)

    pesi = weight

    h1 = ROOT.TH1F('h1',' ',70,0.85, 1.01)
    h2 = ROOT.TH1F('h2',' ',70,0.85, 1.01)
    h3 = ROOT.TH1F('h3',' ',70,0.85, 1.01)

    cX = ROOT.TCanvas( 'cx', 'Projection Canvas in X', 730, 10, 700, 500 )


    if(myvar=="E"):
        var = "gamma_PP_IsNotE"
        loc = "right"
    if(myvar=="H"):
        var = "gamma_PP_IsNotH"
        loc = "left"




        treemc.Draw(varmc +">>h2", "1")
    tree.Draw(var +">>h1", pesi, "same")
    print varresmc

    #treemc.Draw(varmc +">>h2", "1", "same")
    treemc.Draw(varresmc +">>h3", "1", "same")




    h1.Sumw2()
    h2.Sumw2()
    h3.Sumw2()


    h1.Scale(1./h1.Integral(),"width")
    h1.GetXaxis().SetTitle(var)
    h1.SetLineColor(ROOT.kBlack)
    h1.SetLineWidth(2)

    h2.Scale(1./h2.Integral(),"width")
    h2.GetXaxis().SetTitle(var)
    h2.SetLineColor(ROOT.kRed)
    h2.SetLineWidth(2)

    h3.Scale(1./h3.Integral(),"width")
    h3.GetXaxis().SetTitle(var)
    h3.SetLineColor(ROOT.kBlue)
    h3.SetLineWidth(2)

    if(myvar=="E"):
        h1.SetMaximum(4.0)
        h2.SetMaximum(4.0)
        h3.SetMaximum(4.0)


    if(loc == "right"):
        legend = ROOT.TLegend(.6,.77,.9,.9)
    else:
        legend = ROOT.TLegend(.1,.75,.4,.9)


    legend.AddEntry(h1, "data kst sample")
    legend.AddEntry(h2, "MC")
    legend.AddEntry(h3, "MC resampling ")


    legend.Draw()
    cX.SaveAs("./plots" + name + myvar +"_nTracks_ETA_v5" +".pdf")


#plotcompare("E", "KstGamma_S21Data_sWeights.root", "nkstgWeight", "DecayTree", "kstg2_final_plotbins", "IsNotH_Res_final")
#plotcompare("H", "KstGamma_S21Data_sWeights.root", "nkstgWeight", "DecayTree", "kstg2_final2_after", "IsNotH_Res_after")

plotcompare("H", "KstGamma_S34Data_sWeights.root", "KstGamma_S34MC.root", "nSigWeight", "DecayTree", "bli", "gamma_PP_IsNotH", "gamma_PP_IsNotH", "IsNotH_Res_nTracks_ETA")
#plotcompare("E", "KstGamma_S21Data_sWeights.root", "AllKstGamma_S21MC.root", "nkstgWeight", "DecayTree", "blo", "gamma_PP_IsNotE", "gamma_PP_IsNotE", "IsNotE_Res")

#python -i apply_histos.py --energy_range range5 --treename "DecayTree" --histosfile ref_histos_IsNotH_v2.root --newvar IsNotE_Res_v2 --filename KstGamma_S21Data_sWeights.root
