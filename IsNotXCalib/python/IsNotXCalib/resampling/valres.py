import os
import ROOT
ROOT.gROOT.SetBatch()
ROOT.gSystem.Load('libRooFit')

from ROOT import gStyle
from math import *
gStyle.SetOptStat(0000000000)

def plotcompare(myvar, data_file, weight, tree, name, varres):

    f = ROOT.TFile.Open(data_file)
    tree = f.Get(tree)
    pesi = weight

    h1 = ROOT.TH1F('h1',' ',60,0.0, 1.1)
    h2 = ROOT.TH1F('h2',' ',60,0.0, 1.1)

    cX = ROOT.TCanvas( 'cx', 'Projection Canvas in X', 730, 10, 700, 500 )


    if(myvar=="E"):
        var = "gamma_PP_IsNotE"
        loc = "right"
    if(myvar=="H"):
        var = "gamma_PP_IsNotH"
        loc = "left"





    tree.Draw(var +">>h1", pesi)
    tree.Draw(varres +">>h2", pesi, " same")



    h1.Sumw2()
    h2.Sumw2()


    h1.Scale(1./h1.Integral(),"width")
    h1.GetXaxis().SetTitle(var)
    h1.SetLineColor(ROOT.kBlue)
    h1.SetLineWidth(2)

    h2.Scale(1./h2.Integral(),"width")
    h2.GetXaxis().SetTitle(var)
    h2.SetLineColor(ROOT.kMagenta)
    h2.SetLineWidth(2)


    if(myvar=="E"):
        h1.SetMaximum(2.8)
        h2.SetMaximum(2.8)

    if(loc == "right"):
        legend = ROOT.TLegend(.6,.77,.9,.9)
    else:
        legend = ROOT.TLegend(.1,.75,.4,.9)


    legend.AddEntry(h1, "data calibration sample")
    legend.AddEntry(h2, "resampling ")

    legend.Draw()
    cX.SaveAs("./plots" + name + myvar +".pdf")


#plotcompare("E", "KstGamma_S21Data_sWeights.root", "nkstgWeight", "DecayTree", "kstg2_final_plotbins", "IsNotH_Res_final")
#plotcompare("H", "KstGamma_S21Data_sWeights.root", "nkstgWeight", "DecayTree", "kstg2_final2_after", "IsNotH_Res_after")

plotcompare("H", "Dsstr2DsGamma_S21Data_2012_sWeights.root", "nSigWeight", "Tree", "dstr", "IsNotH_Res_after")
#python -i apply_histos.py --energy_range range5 --treename "DecayTree" --histosfile ref_histos_IsNotH_v2.root --newvar IsNotE_Res_v2 --filename KstGamma_S21Data_sWeights.root
