#!/usr/bin/env python
# =============================================================================
# @file   make_histos.py
# @author Clara Remon Alepuz (cremonal@cern.ch)
# @based  on  Vicente Rives code
# @date   28.03.2020
# =============================================================================
"""Create reference histograms for resampling the isNotX variable."""

import os
import sys
import array

import ROOT

from IsNotXCalib.binning import load_binning_from_txt
from IsNotXCalib.defaults import add_file_info, apply_defaults
from IsNotXCalib.utils import pairwise

def init_reference_histos(bins_pt, bins_eta, args):
    """Initialize reference histograms.

    The provided binning consists on the edges of the bins.

    :param bins: Edges of the bins.
    :type bins: list

    :returns: Histograms as (low_bin, TH1F) pairs
    :rtype: dict

    """
    histos = {}
    if(args.IsNotX == "IsNotH"):
        bin_scheme = [0.0,0.03, 0.06, 0.1, 0.3, 0.4, 0.5, 0.55, 0.6,0.65, 0.7, 0.72, 0.75, 0.78, 0.8, 0.83, 0.86, 0.89, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.9725,0.9755, 0.9785,0.98, 0.985,0.9875,0.99, 0.9925,0.9945, 0.9965, 1.0]
    else:
        bin_scheme = [0.0, 0.09, 0.11, 0.13, 0.16, 0.18, 0.2, 0.225, 0.235,0.25, 0.275, 0.3, 0.33, 0.36, 0.37, 0.4, 0.43, 0.46, 0.5, 0.53, 0.56, 0.59, 0.63, 0.65, 0.7, 0.75, 0.77, 0.8, 0.82, 0.84, 0.86, 0.88, 0.9, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.985, 0.99, 0.9925,0.9945, 0.9965, 1.0]

    bin_edges = array.array('f', bin_scheme)
    for low_pt, high_pt in pairwise(bins_pt):
        for low_eta, high_eta in pairwise(bins_eta):
            bin_name = "[%.2f,%.2f,%.2f,%.2f]" % (low_pt, high_pt, low_eta, high_eta)
            histos[str(low_pt)+'_'+str(low_eta)] = ROOT.TH1F(bin_name, bin_name, len(bin_scheme)-1, bin_edges)
    return histos


def sanitize_histos(histos):
    """Sanitize histograms by setting negative bins to zero.

    :param histos: Histograms to sanitize.
    :type histos: dict

    :returns: Number of sanitized bins.
    :rtype: int

    """
    n_changes = 0
    n_bins = 0
    for histo in histos.values():
        for bin_number in range(1, histo.GetNbinsX()+1):
            if histo.GetBinContent(bin_number) < 0:
                histo.SetBinContent(bin_number, 0.0)
                print "Sanitized histograms", histo
                n_changes += 1
                n_bins += 1
            else:
                n_bins += 1


    return n_changes, n_bins


def make_reference_histos(tree, calib_var, bin_var, weight_var, bins_pt, bins_eta, args):
    """Fill the reference histograms from the reference tree.

    :param tree: Tree to get the values from.
    :type tree: ROOT.TTree
    :param calib_var: Variable to resample.
    :type calib_var: str
    :param bin_var_pt: Variable name for the PT binning.
    :type bin_var_pt: str
    :param bin_var_eta: Variable name for the eta binning.
    :type bin_var_eta: str
    :param weight_var: Variable in the tree to be used as weight.
    :type weight_var: str
    :param bins_pt: Bin edges for the bin_var_pt.
    :type bins_pt: list
    :param bins_eta: Bin edges for the bin_var_eta.
    :type bins_eta: list
    :param args:

    :returns: Reference histograms as (low_bin, TH1F) pairs
    :rtype: dict

    :raise: ValueError: If reference histograms cannot be initialized.

    """
    # Initialize reference histograms

    histos_ref = init_reference_histos(bins_pt, bins_eta, args)
    if not histos_ref:
        print "Problem initializing reference histograms"
        sys.exit(1)
    # Loop!
    for _ in tree:
        # Load values from leaves
        calib_var_val = getattr(tree, calib_var)
        bin_var_pt_val = getattr(tree, bin_var[0])
        bin_var_eta_val = getattr(tree, bin_var[1])
        if (args.isphoton_var=="none"):
            isphoton_val = "1.0"
        else:
            isphoton_val = getattr(tree, args.isphoton_var)        
        isnoth_val = getattr(tree, args.isnoth_var)
        isnote_val = getattr(tree, args.isnote_var)

        if weight_var:
            weight_var_val = getattr(tree, weight_var)
        else:
            weight_var_val = 1
        for low_pt, high_pt in pairwise(bins_pt):
            for low_eta, high_eta in pairwise(bins_eta):
                # Find the bin the bin variable sits in
                if (bin_var_pt_val >= low_pt) and (bin_var_pt_val < high_pt):
                    if (bin_var_eta_val >= low_eta) and (bin_var_eta_val < high_eta):
                        if(args.IsNotX=="IsNotE"):
                            if args.stripping_version == 'S20':
                                if isphoton_val > args.isphoton_cut and isnoth_val > args.isnoth_cut and isnoth_val != 0.5:
                                    histos_ref[str(low_pt)+'_'+str(low_eta)].Fill(calib_var_val, weight_var_val)
                                    break
                            else:
                                if isphoton_val > args.isphoton_cut and isnoth_val > args.isnoth_cut:
                                    histos_ref[str(low_pt)+'_'+str(low_eta)].Fill(calib_var_val, weight_var_val)
                                    break
                        if(args.IsNotX=="IsNotH"):
                            if isphoton_val > args.isphoton_cut and isnote_val > args.isnote_cut:
                                histos_ref[str(low_pt)+'_'+str(low_eta)].Fill(calib_var_val, weight_var_val)
                                break

    n_sanitizations, nbins = sanitize_histos(histos_ref)
    if n_sanitizations > 0:
        print "I have sanitized %s bins of a total of %s bins, please make sure that the binning is correct" %(n_sanitizations, nbins)
    return histos_ref


if __name__ == '__main__':
    import argparse
    # Define parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--usemc', action='store_true', help='Use the MC calibration samples')

    parser.add_argument('--binfile', type=str, help='File containing the binning for the transverse momentum and pseudorapidity')
    parser.add_argument('-e', '--energy_range', type=str, default = 'range5', dest='energy_range',
                        help='Energy range', choices = ['range1', 'range2', 'range3', 'range4', 'range5', 'user', 'range6', 'range7', 'range8_ptmin', 'range8_ptmax','range9'])
    parser.add_argument('--stripping', type=str, default='S21', dest='stripping_version',
                        help='Stripping version of the calibration samples', choices = ['S20', 'S21', 'TURCAL','S24', 'S28', 'S29', 'S34'])

    parser.add_argument('--var_eff', type=str, default='IsNotH', dest='IsNotX',
                                                                        help='Variable to compute the efficiencies', choices = ['IsNotH', 'IsNotE'])
    parser.add_argument('--isnothcut', type=float, default = 0.0, dest='isnoth_cut',
                            help='Cuts to apply to the IsNotH variable')
    parser.add_argument('--isnotecut', type=float, default = 0.0, dest='isnote_cut',
                            help='Cuts to apply to the IsNotE variable')
    parser.add_argument('--isphotoncut', type=float, default=0.0, dest='isphoton_cut',
                        help='Cut to apply to the isPhoton variable')

    parser.add_argument('--binvar_1', type=str, default='PT', dest='bin1',
    help='Binning variable 1', choices = ['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks'])
    parser.add_argument('--binvar_2', type=str, default='ETA', dest='bin2',
    help='Binning variable 2', choices = ['NSPDHits', 'PT', 'ETA', 'NPVs', 'NTracks'])

    # Parse
    args        = parser.parse_args()
    erange    = args.energy_range
    args        = apply_defaults(add_file_info(args, erange), erange, 'make')
    file_name   = args.input_file_name
    tree_name   = args.input_tree_name
    if(args.IsNotX=="IsNotE"):
        var         = args.vare
    else:
        var         = args.varh

    bin_file    = args.binfile
    weight_var  = args.weight_var
    stripping   = args.stripping_version

    binvars    = [args.bin1,args.bin2]
    bin_var = ['', '']
    for i in [0,1]:
        if(binvars[i] == 'PT'):
            bin_var[i] = args.pt_var
        elif(binvars[i] == 'ETA'):
            bin_var[i] = args.eta_var
        elif(binvars[i] == 'NSPDHits'):
            bin_var[i] = args.nspdhits_var
        elif(binvars[i] == 'NPVs'):
            bin_var[i] = args.npvs_var
        elif(binvars[i] == 'NTracks'):
            bin_var[i] = args.ntracks_var



    # Open and check reference file
    if not os.path.exists(file_name):
        print "Cannot find the file with the reference tree -> %s" % file_name
        sys.exit(1)
    ref_file = ROOT.TFile(file_name, 'r')
    ref_tree = ref_file.Get(tree_name)
    if not ref_tree:
        print "The reference file does not contain the specified tree -> %s" % tree_name
        sys.exit(1)
    # Get binning
    try:
        bins_pt, bins_eta  = load_binning_from_txt(bin_file, binvars)#tochange
    except OSError:
        print "Cannot load binning scheme from %s" % bin_file
        sys.exit(1)
    # Fill reference histograms
    bins_pt.sort()
    bins_eta.sort()
    ref_histos = make_reference_histos(ref_tree, var, bin_var, weight_var, bins_pt, bins_eta, args)
    # Save the histograms into a file
    output_file = str(args.IsNotX) + "_ref_histos_nspdhits.root"
    histo_file = ROOT.TFile(output_file, 'RECREATE')
    for histos in ref_histos.values():
        histos.Write()
    # Write file and close
    histo_file.Write()
    histo_file.Close()
    ref_file.Close()
    # Print and exit
    print "All went well. Reference histograms have been placed in %s, enjoy!" % output_file

# EOF
