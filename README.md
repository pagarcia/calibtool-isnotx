# IsNotXCalib tool

IsNotE and IsNotH gamma PID variables allow to separate photons from electrons and hadrons respectively (https://indico.cern.ch/event/226072/contributions/1534618/attachments/371765/517310/neutralID.pdf). This useful variable to reject background when photons are involve are not well reproduced in the simulation.  This disagreement between real data and MC implies the incorrect computation of efficiencies for cuts in these variables. This calibration tool allows to extract the efficiency using calibration samples in different energy ranges. With this purpose, two different approaches are included within the Calibration Tool and the Resampling Tool. The first one allows to extract the efficiency of a given IsNotX (IsNotE or IsNotH) cut in 2D bins (transverse momentum and pseudo-rapidity of the photon are used by default) and also to add this as a weight to your own sample. The second procedure allows to resample the IsNotX variable so the MC distribution matches the data one, using as reference the calibration sample. Both tools consider that the transverse momentum and pseudo-rapidity of the photon are well reproduced in MC. The usage details of this tools are explained in the following.

## Getting the package and setup
```
lb-set-platform x86_64-slc6-gcc62-opt
lb-dev Urania/v7r0
cd UraniaDev_v7r0
git lb-use Urania
git lb-checkout Urania/master IsNotXCalib (^)
git checkout Urania/master cmake
ln -s `which xenv` cmake
make
./run bash --norc
```
(^) when package is added to Urania


## Calibration samples
Several calibration samples are available to cover different energy ranges of the photon. The selection of the calibration sample can be done by the input argument `--energy_range`. These samples for RUN I are stored in path: `/eos/lhcb/wg/CaloObj/CalibSamples/S21`. The samples for RUN II are stored in path: `/afs/cern.ch/user/c/chefdevi/eoscaloobj/CalibSamples/Validation/Fit2/AddEta/` for S24, S28, S29, S34 and in `/afs/cern.ch/work/p/pagarcia/public/` for TURCAL.

### Calibration samples for RUN I (S21):

 | Calibration channel  | Available variables & samples | Photon PT range          | Statistics (signal events) | `--energy_range`|  `--stripping` |
 | -------------------  | ----------------- | ------------------------ | -------------------------- | --------------- | --------------
 | Bs2JpsiEta           | ETA, PT, NPVs, NTracks and NSPDHits (Data and MC)       | [0, 4000]                | 1530    (2012)             | range1          |  S21|
 | Bu2JpsiKstr          | ETA, PT, NPVs, NTracks and NSPDHits (Data and MC)       | [0, 2500]                | 17170   (Run I)            | range2          | S21 |
 | Ds2EtapPi            | ETA, PT, NPVs, NTracks and NSPDHits (Data)              | [1000, 4000]             | 473450  (2012, MagDown)    | range3          |  S21 |
 | Dsstr2DsGamma        | ETA, PT, NPVs, NTracks and NSPDHits (Data)              | [500, 2000]              | 620045  (2012)             | range4          |  S21 |
 | KstGamma             | ETA, PT, NPVs, NTracks and NSPDHits (Data and MC)       | [3000, 10000]            | 36260   (Run I)            | range5          |  S21 |

### Calibration samples for Run II : 

 | Calibration channel  | Available variables & samples| Photon PT range          | Statistics (signal events) | `--energy_range`| `--stripping` |
 | -------------------  | ----------------------------- | ------------------------ | -------------------------- | --------------- | ------------------- |
 | Ds2EtapPi            | ETA, PT, NTracks (Data)       | [500, 7000]              | 12003140 (Run II)          | range3          | TURCAL             |
 | KstGamma             | ETA, PT, NPVs, NTracks and NSPDHits (Data)  | [2500, 20000]               | 27607   (Run II)              | range5          | S24 |
 | KstGamma             | ETA, PT, NPVs, NTracks and NSPDHits (Data)              | [2500, 20000]            | 146050   (Run II)             | range5          | S28 |
 | KstGamma             | ETA, PT, NPVs, NTracks and NSPDHits (Data and MC)       | [3000, 20000]            | 137644   (Run II)             | range5          | S29 |
 | KstGamma             | ETA, PT, NPVs, NTracks and NSPDHits (Data and MC)       | [2500, 20000]            | 171047   (Run II)             | range5          | S34 |
 | Eta2MuMuGamma        | ETA, PT, NTracks (Data)              | [0, 8000]                | 13115410  (Run II)         | range6          | TURCAL    |
 | Dst2D0piM            | ETA, PT, NTracks (Data)              | [2000, 15000]            | 28398960  (Run II)         | range7          | TURCAL    |
 | Dst2D0piR (PT min)   | ETA, PT, NTracks (Data)              | [200, 2000]              | 90626800   (Run II)        | range8_ptmin          | TURCAL    | 
 | Dst2D0piR  (PT max)  | ETA, PT, NTracks (Data)              | [400, 6000]              | 90626800   (Run II)        | range8_ptmax          | TURCAL    |
 | Dsst2DsGamma | ETA, PT, NTracks (Data)  | [250, 2500]              | 90626800   (Run II)        | range9         | TURCAL    |
 
## User calibration sample
If desired, the user can also use their own sample by choosing `--energy_range user`. The information needs to be added in a file called `calibrationsample_info.txt` in `IsNotXCalib/python/IsNotXCalib/` as follows:
```
{ "data":{ "file_name": "YourCalibrationSample_data.root",
          "tree_name": "Tree",
          "isphoton_var": "gamma_PP_IsPhoton",
          "isnoth_var": "gamma_PP_IsNotH",
          "isnote_var": "gamma_PP_IsNotE",
          "PT": "gamma_PT",
          "ETA": "gamma_ETA",
          "NSPDHits": "nSPDHits",
          "weight_var": "nSigWeight"},
  "mc": { "file_name": "YourCalibrationSample_mc.root"
          "tree_name": "DecayTree",
          "isphoton_var": "gamma_PP_IsPhoton",
          "isnoth_var": "gamma_PP_IsNotH",
          "isnote_var": "gamma_PP_IsNotE",
          "PT": "gamma_PT",
          "ETA": "gamma_ETA",
          "weight_var": "1"}
        }
```
There is an example file here: `IsNotXCalib/python/IsNotXCalib/calibrationsample_info.txt`


## Binning variables and scheme
With this tool the efficiency of the IsNotX variable can be study in terms of transverse momentum and pseudorapidity bins. However they can be also dependent on other variables as `NSPDHits`, `NTracks` and `NPVs`, and the user can choose two binning variables among all of them to compute the efficiencies. The tool uses default binning schemes for each calibration sample, but the binning scheme can also be provided by the user, by adding a file similar to `IsNotXCalib/options/binning_calibration_range5.txt`, and introduce it in the arguments as it is specified in the following sections.

## Input arguments
* `--energy_range` : Allows to select the calibration sample depending on the photon energy range (choices: `range1`, `range2`, `range3`, `range4`, `range5` for RUN I and `range3`, `range5`, `range6`, `range7`, `range8_ptmin`, `range8_ptmax`, `range9` for RUN II. Also, the option `user` is available for both RUN I and RUN II)
* `--stripping` : Allows to select the stripping version of the calibration sample. (choices: `S21`, `S24`, `S28`, `S29`, `S34`, `TURCAL` and the default is `S21`)
* `--var_eff`      : This parameter selects which IsNotX variable we want to study, and the efficiencies will be computed for the variable we choose here. In order to take into account the correlations between the efficiencies of the different photon PID variables, this tool gives the possibility of including cuts on the other IsNotX and IsPhoton variables, in order to compute the efficiencies on top of that. (choices: `IsNotH`, `IsNotE`)
* `--isnothcut`    : To introduce the cut value on IsNotH. If IsNotH is selected as the `--var_eff`, efficiencies will be calculated for this cut. Otherwise, the efficiencies will be computed on top of this cut (default 0.0)
* `--isnotecut`    : To introduce the cut value on IsNotE. If IsNotE is selected as the `--var_eff`, efficiencies will be calculated for this cut. Otherwise, the efficiencies will be computed on top of this cut (default 0.0)
* `--isphotoncut`  : To introduce the cut value on IsPhoton. Efficiencies will be computed on top of this cut (default 0.0)
* `--binvar_1 `    : This parameter is useful to select the binning variables that will be used to compute the efficiencies (choices: `PT`, `ETA`, `NSPDHits`, `NTracks`, `NPVs` and the default is `PT`)
* `--binvar_2`     : This parameter is useful to select the binning variables that will be used to compute the efficiencies (choices: `PT`, `ETA`, `NSPDHits`, `NTracks`, `NPVs` and the default is ETA)
* `--binfile `     : A binning scheme is required by the different tools, and this can be provided by the user. (i.e `--binfile 'YourBinningScheme.txt'`)

## Calibration Tool
This tool allows to study the IsNotX efficiency by different scripts with different features and usage:

* `build_efficiency_tables.py` : This takes as input the cuts on IsNotX and IsPhoton, and gives as output the efficiency of the cut on the IsNotX variables that has been selected as `--var_eff`. Efficiencies are given in 2D bins of PT and ETA by default, or the binning variables that are selected by the user. The output format can be also selected (the options being text, python-like and cpp-like).
An example of usage:  

```
python -i build_efficiency_tables.py --energy_range range3 --isnothcut 0.25 --isphotoncut 0.6 --output text --output python --binvar_2 NSPDHits --var_eff IsNotH
```


* `build_efficiency_tables.py` : This script allows to plot the efficiency of a given IsNotX cut in terms of `PT`, `ETA`, `NSPDHits`, `NTracks` or `NPVs`. Also in 2D bins of the binning variables selected or the default ones (`PT` and `ETA`). Also the efficiency depending on the cut applied on IsNotX can be studied. To do this, `--plot_type` can be set to `PT`, `ETA`, `NSPDHits`, `NTracks` or `NPVs`, `2D` or `isnotx`. If `isnotx` is selected a plot of the `--var_eff` efficiency for different cuts is provided.
It can be used as follows:

```
python -i make_efficiency_plots.py  --isnothcut 0.25  --var_eff IsNotH --plot_type NSPDHits --binvar_1 NSPDHits
```

* `add_weight_to_ntuple.py` : Add a weight with the efficiency of the IsNotX to the original tree, as follows:

```
python -i add_weight_to_ntuple.py --mcweightname IsNotHWeight --var_eff IsNotH --binvar_2 NSPDHits yourtuple.root DecayTree 'gamma_PT' 'nSPDHits' 
```
where the last three arguments are the name of the tree and the name of the binning variables in the tuple (`yourtuple.root`), where the weights will be added.

## Resampling Tool
This is a different approach to study the IsNotX efficiencies, which assumes that distribution of the binning variables are well reproduced in the simulation. Using a certain binning tool assigns a new value for the IsNotX to each event by selecting a random value extracted from the real data distribution. This is done in two steps, first with the `make_histos.py` script the reference histograms are produced using the calibration sample. The usage is the following:

```
python -i make_histos.py --energy_range range2 --binvar_2 NSPDHits --var_eff IsNotH 
```

or in case you want to use your own calibration sample and your own binning scheme:

```
python -i make_histos.py --binfile 'Your_binning_resampling.txt' --var_eff IsNotE --binvar_2 NSPDHits
```

This gives as output the reference histograms in a `.root` file, that will be used by `apply_histos.py` to add the resampled IsNotX variable to your tuple:

```
python -i apply_histos.py --energy_range range4 --treename Tree --histosfile ref_histos_IsNotH.root --newvar IsNotE_Res --bin1_name gamma_PT --bin2_name gamma_ETA --filename YourTuple.root
```

where `--treename` is the name of the tree in the tuple we want to resample, `--filename` is the name of this tuple, and `--newvar` is to choose the name of the new variable. `--bin1_name` and `--bin2_name` are the names in your tuple that have the binning variables.
